
import { updateVersions } from "sk-cl-tools";

// npm --include-repos=.*-alert-.* run vbump
let includeMask = process.env.npm_config_include_repos ? process.env.npm_config_include_repos : ".*";
// npm --include-names=.*-alert-.* run vbump
let nameMask = process.env.npm_config_include_names ? process.env.npm_config_include_names : "sk-*";
// npm --include-names=.*-alert-.* run vbump --test-only=yes
let testOnly = process.env.npm_config_test_only ? process.env.npm_config_test_only : false;
// npm run vbump --include-repos=.*gridy.* --include-names=sk-.* --test-only=yes --to-version=0.2.27
let version = process.env.npm_config_to_version ? process.env.npm_config_to_version : false;


if (version) {
    updateVersions('../', version, testOnly, nameMask, includeMask);
} else {
    console.warn('No version specified')
}


