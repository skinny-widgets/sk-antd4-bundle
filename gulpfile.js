let gulp = require('gulp');
let run = require('gulp-run');

let paths = {
    packages: {
        src: [
            '../sk-core/src/**/*.js',
            '../sk-input/src/**/*.js',
            '../sk-input-antd4/**/*.js',
            '../sk-button/src/**/*.js',
            '../sk-button-antd4/**/*.js',
            '../sk-alert/src/**/*.js',
            '../sk-alert-antd4/**/*.js',
            '../sk-accordion/src/**/*.js',
            '../sk-accordion-antd4/**/*.js',
            '../sk-app/src/**/*.js',
            '../sk-app-antd4/**/*.js',
            '../sk-checkbox/src/**/*.js',
            '../sk-checkbox-antd4/**/*.js',
            '../sk-datepicker/src/**/*.js',
            '../sk-datepicker-antd4/**/*.js',
            '../sk-dialog/src/**/*.js',
            '../sk-dialog-antd4/**/*.js',
            '../sk-form/src/**/*.js',
            '../sk-form-antd4/**/*.js',
            '../sk-navbar/src/**/*.js',
            '../sk-navbar-antd4/**/*.js',
            '../sk-select/src/**/*.js',
            '../sk-select-antd4/**/*.js',
            '../sk-spinner/src/**/*.js',
            '../sk-spinner-antd4/**/*.js',
            '../sk-switch/src/**/*.js',
            '../sk-switch-antd4/**/*.js',
            '../sk-tabs/src/**/*.js',
            '../sk-tabs-antd4/**/*.js',
            '../sk-tree/src/**/*.js',
            '../sk-theme-antd4/**/*'
        ],
        dest: './node_modules'
    },
    stand: {
        dest: '../sk-stand/node_modules/sk-antd-bundle/dist'
    }
};

function rebuild() {
    return run('npm run build').exec();
}

function scripts() {
    return gulp.src(paths.packages.src)
        .pipe(gulp.dest(paths.packages.dest));
}

function hotdeploy() {
    return gulp.src('./dist')
        .pipe(gulp.dest(paths.stand.dest));
}

/**
 * watches set of bundle source packages, rebuild bundle and hotdeploy (replace files) to stand
 * when changed
 */
function watch() {
    gulp.watch(paths.packages.src, gulp.series(scripts, rebuild, hotdeploy));
}

exports.watch = watch;
