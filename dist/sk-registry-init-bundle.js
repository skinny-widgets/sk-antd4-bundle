(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.window = global.window || {}));
}(this, (function (exports) { 'use strict';

  function _newArrowCheck(n, r) {
    if (n !== r) throw new TypeError("Cannot instantiate an arrow function");
  }

  function _classCallCheck(a, n) {
    if (!(a instanceof n)) throw new TypeError("Cannot call a class as a function");
  }

  function _typeof(o) {
    "@babel/helpers - typeof";

    return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, _typeof(o);
  }

  function toPrimitive(t, r) {
    if ("object" != _typeof(t) || !t) return t;
    var e = t[Symbol.toPrimitive];
    if (void 0 !== e) {
      var i = e.call(t, r || "default");
      if ("object" != _typeof(i)) return i;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return ("string" === r ? String : Number)(t);
  }

  function toPropertyKey(t) {
    var i = toPrimitive(t, "string");
    return "symbol" == _typeof(i) ? i : i + "";
  }

  function _defineProperties(e, r) {
    for (var t = 0; t < r.length; t++) {
      var o = r[t];
      o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, toPropertyKey(o.key), o);
    }
  }
  function _createClass(e, r, t) {
    return r && _defineProperties(e.prototype, r), t && _defineProperties(e, t), Object.defineProperty(e, "prototype", {
      writable: !1
    }), e;
  }

  function _assertThisInitialized(e) {
    if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    return e;
  }

  function _possibleConstructorReturn(t, e) {
    if (e && ("object" == _typeof(e) || "function" == typeof e)) return e;
    if (void 0 !== e) throw new TypeError("Derived constructors may only return object or undefined");
    return _assertThisInitialized(t);
  }

  function _getPrototypeOf(t) {
    return _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function (t) {
      return t.__proto__ || Object.getPrototypeOf(t);
    }, _getPrototypeOf(t);
  }

  function _setPrototypeOf(t, e) {
    return _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function (t, e) {
      return t.__proto__ = e, t;
    }, _setPrototypeOf(t, e);
  }

  function _inherits(t, e) {
    if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
    t.prototype = Object.create(e && e.prototype, {
      constructor: {
        value: t,
        writable: !0,
        configurable: !0
      }
    }), Object.defineProperty(t, "prototype", {
      writable: !1
    }), e && _setPrototypeOf(t, e);
  }

  function _arrayLikeToArray$6(r, a) {
    (null == a || a > r.length) && (a = r.length);
    for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e];
    return n;
  }

  function _arrayWithoutHoles(r) {
    if (Array.isArray(r)) return _arrayLikeToArray$6(r);
  }

  function _iterableToArray(r) {
    if ("undefined" != typeof Symbol && null != r[Symbol.iterator] || null != r["@@iterator"]) return Array.from(r);
  }

  function _unsupportedIterableToArray$6(r, a) {
    if (r) {
      if ("string" == typeof r) return _arrayLikeToArray$6(r, a);
      var t = {}.toString.call(r).slice(8, -1);
      return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$6(r, a) : void 0;
    }
  }

  function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  function _toConsumableArray(r) {
    return _arrayWithoutHoles(r) || _iterableToArray(r) || _unsupportedIterableToArray$6(r) || _nonIterableSpread();
  }

  function asyncGeneratorStep(n, t, e, r, o, a, c) {
    try {
      var i = n[a](c),
        u = i.value;
    } catch (n) {
      return void e(n);
    }
    i.done ? t(u) : Promise.resolve(u).then(r, o);
  }
  function _asyncToGenerator(n) {
    return function () {
      var t = this,
        e = arguments;
      return new Promise(function (r, o) {
        var a = n.apply(t, e);
        function _next(n) {
          asyncGeneratorStep(a, r, o, _next, _throw, "next", n);
        }
        function _throw(n) {
          asyncGeneratorStep(a, r, o, _next, _throw, "throw", n);
        }
        _next(void 0);
      });
    };
  }

  function _isNativeFunction(t) {
    try {
      return -1 !== Function.toString.call(t).indexOf("[native code]");
    } catch (n) {
      return "function" == typeof t;
    }
  }

  function _isNativeReflectConstruct$7() {
    try {
      var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
    } catch (t) {}
    return (_isNativeReflectConstruct$7 = function _isNativeReflectConstruct() {
      return !!t;
    })();
  }

  function _construct(t, e, r) {
    if (_isNativeReflectConstruct$7()) return Reflect.construct.apply(null, arguments);
    var o = [null];
    o.push.apply(o, e);
    var p = new (t.bind.apply(t, o))();
    return r && _setPrototypeOf(p, r.prototype), p;
  }

  function _wrapNativeSuper(t) {
    var r = "function" == typeof Map ? new Map() : void 0;
    return _wrapNativeSuper = function _wrapNativeSuper(t) {
      if (null === t || !_isNativeFunction(t)) return t;
      if ("function" != typeof t) throw new TypeError("Super expression must either be null or a function");
      if (void 0 !== r) {
        if (r.has(t)) return r.get(t);
        r.set(t, Wrapper);
      }
      function Wrapper() {
        return _construct(t, arguments, _getPrototypeOf(this).constructor);
      }
      return Wrapper.prototype = Object.create(t.prototype, {
        constructor: {
          value: Wrapper,
          enumerable: !1,
          writable: !0,
          configurable: !0
        }
      }), _setPrototypeOf(Wrapper, t);
    }, _wrapNativeSuper(t);
  }

  function _arrayWithHoles(r) {
    if (Array.isArray(r)) return r;
  }

  function _iterableToArrayLimit(r, l) {
    var t = null == r ? null : "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"];
    if (null != t) {
      var e,
        n,
        i,
        u,
        a = [],
        f = !0,
        o = !1;
      try {
        if (i = (t = t.call(r)).next, 0 === l) {
          if (Object(t) !== t) return;
          f = !1;
        } else for (; !(f = (e = i.call(t)).done) && (a.push(e.value), a.length !== l); f = !0);
      } catch (r) {
        o = !0, n = r;
      } finally {
        try {
          if (!f && null != t["return"] && (u = t["return"](), Object(u) !== u)) return;
        } finally {
          if (o) throw n;
        }
      }
      return a;
    }
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  function _slicedToArray(r, e) {
    return _arrayWithHoles(r) || _iterableToArrayLimit(r, e) || _unsupportedIterableToArray$6(r, e) || _nonIterableRest();
  }

  function createCommonjsModule(fn) {
    var module = { exports: {} };
  	return fn(module, module.exports), module.exports;
  }

  var _typeof_1 = createCommonjsModule(function (module) {
    function _typeof(o) {
      "@babel/helpers - typeof";

      return module.exports = _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
        return typeof o;
      } : function (o) {
        return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
      }, module.exports.__esModule = true, module.exports["default"] = module.exports, _typeof(o);
    }
    module.exports = _typeof, module.exports.__esModule = true, module.exports["default"] = module.exports;
  });

  var regeneratorRuntime$1 = createCommonjsModule(function (module) {
    var _typeof = _typeof_1["default"];
    function _regeneratorRuntime() {

      /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */
      module.exports = _regeneratorRuntime = function _regeneratorRuntime() {
        return e;
      }, module.exports.__esModule = true, module.exports["default"] = module.exports;
      var t,
        e = {},
        r = Object.prototype,
        n = r.hasOwnProperty,
        o = Object.defineProperty || function (t, e, r) {
          t[e] = r.value;
        },
        i = "function" == typeof Symbol ? Symbol : {},
        a = i.iterator || "@@iterator",
        c = i.asyncIterator || "@@asyncIterator",
        u = i.toStringTag || "@@toStringTag";
      function define(t, e, r) {
        return Object.defineProperty(t, e, {
          value: r,
          enumerable: !0,
          configurable: !0,
          writable: !0
        }), t[e];
      }
      try {
        define({}, "");
      } catch (t) {
        define = function define(t, e, r) {
          return t[e] = r;
        };
      }
      function wrap(t, e, r, n) {
        var i = e && e.prototype instanceof Generator ? e : Generator,
          a = Object.create(i.prototype),
          c = new Context(n || []);
        return o(a, "_invoke", {
          value: makeInvokeMethod(t, r, c)
        }), a;
      }
      function tryCatch(t, e, r) {
        try {
          return {
            type: "normal",
            arg: t.call(e, r)
          };
        } catch (t) {
          return {
            type: "throw",
            arg: t
          };
        }
      }
      e.wrap = wrap;
      var h = "suspendedStart",
        l = "suspendedYield",
        f = "executing",
        s = "completed",
        y = {};
      function Generator() {}
      function GeneratorFunction() {}
      function GeneratorFunctionPrototype() {}
      var p = {};
      define(p, a, function () {
        return this;
      });
      var d = Object.getPrototypeOf,
        v = d && d(d(values([])));
      v && v !== r && n.call(v, a) && (p = v);
      var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p);
      function defineIteratorMethods(t) {
        ["next", "throw", "return"].forEach(function (e) {
          define(t, e, function (t) {
            return this._invoke(e, t);
          });
        });
      }
      function AsyncIterator(t, e) {
        function invoke(r, o, i, a) {
          var c = tryCatch(t[r], t, o);
          if ("throw" !== c.type) {
            var u = c.arg,
              h = u.value;
            return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) {
              invoke("next", t, i, a);
            }, function (t) {
              invoke("throw", t, i, a);
            }) : e.resolve(h).then(function (t) {
              u.value = t, i(u);
            }, function (t) {
              return invoke("throw", t, i, a);
            });
          }
          a(c.arg);
        }
        var r;
        o(this, "_invoke", {
          value: function value(t, n) {
            function callInvokeWithMethodAndArg() {
              return new e(function (e, r) {
                invoke(t, n, e, r);
              });
            }
            return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
          }
        });
      }
      function makeInvokeMethod(e, r, n) {
        var o = h;
        return function (i, a) {
          if (o === f) throw Error("Generator is already running");
          if (o === s) {
            if ("throw" === i) throw a;
            return {
              value: t,
              done: !0
            };
          }
          for (n.method = i, n.arg = a;;) {
            var c = n.delegate;
            if (c) {
              var u = maybeInvokeDelegate(c, n);
              if (u) {
                if (u === y) continue;
                return u;
              }
            }
            if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) {
              if (o === h) throw o = s, n.arg;
              n.dispatchException(n.arg);
            } else "return" === n.method && n.abrupt("return", n.arg);
            o = f;
            var p = tryCatch(e, r, n);
            if ("normal" === p.type) {
              if (o = n.done ? s : l, p.arg === y) continue;
              return {
                value: p.arg,
                done: n.done
              };
            }
            "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg);
          }
        };
      }
      function maybeInvokeDelegate(e, r) {
        var n = r.method,
          o = e.iterator[n];
        if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y;
        var i = tryCatch(o, e.iterator, r.arg);
        if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y;
        var a = i.arg;
        return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y);
      }
      function pushTryEntry(t) {
        var e = {
          tryLoc: t[0]
        };
        1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e);
      }
      function resetTryEntry(t) {
        var e = t.completion || {};
        e.type = "normal", delete e.arg, t.completion = e;
      }
      function Context(t) {
        this.tryEntries = [{
          tryLoc: "root"
        }], t.forEach(pushTryEntry, this), this.reset(!0);
      }
      function values(e) {
        if (e || "" === e) {
          var r = e[a];
          if (r) return r.call(e);
          if ("function" == typeof e.next) return e;
          if (!isNaN(e.length)) {
            var o = -1,
              i = function next() {
                for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next;
                return next.value = t, next.done = !0, next;
              };
            return i.next = i;
          }
        }
        throw new TypeError(_typeof(e) + " is not iterable");
      }
      return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", {
        value: GeneratorFunctionPrototype,
        configurable: !0
      }), o(GeneratorFunctionPrototype, "constructor", {
        value: GeneratorFunction,
        configurable: !0
      }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) {
        var e = "function" == typeof t && t.constructor;
        return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name));
      }, e.mark = function (t) {
        return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t;
      }, e.awrap = function (t) {
        return {
          __await: t
        };
      }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () {
        return this;
      }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) {
        void 0 === i && (i = Promise);
        var a = new AsyncIterator(wrap(t, r, n, o), i);
        return e.isGeneratorFunction(r) ? a : a.next().then(function (t) {
          return t.done ? t.value : a.next();
        });
      }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () {
        return this;
      }), define(g, "toString", function () {
        return "[object Generator]";
      }), e.keys = function (t) {
        var e = Object(t),
          r = [];
        for (var n in e) r.push(n);
        return r.reverse(), function next() {
          for (; r.length;) {
            var t = r.pop();
            if (t in e) return next.value = t, next.done = !1, next;
          }
          return next.done = !0, next;
        };
      }, e.values = values, Context.prototype = {
        constructor: Context,
        reset: function reset(e) {
          if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t);
        },
        stop: function stop() {
          this.done = !0;
          var t = this.tryEntries[0].completion;
          if ("throw" === t.type) throw t.arg;
          return this.rval;
        },
        dispatchException: function dispatchException(e) {
          if (this.done) throw e;
          var r = this;
          function handle(n, o) {
            return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o;
          }
          for (var o = this.tryEntries.length - 1; o >= 0; --o) {
            var i = this.tryEntries[o],
              a = i.completion;
            if ("root" === i.tryLoc) return handle("end");
            if (i.tryLoc <= this.prev) {
              var c = n.call(i, "catchLoc"),
                u = n.call(i, "finallyLoc");
              if (c && u) {
                if (this.prev < i.catchLoc) return handle(i.catchLoc, !0);
                if (this.prev < i.finallyLoc) return handle(i.finallyLoc);
              } else if (c) {
                if (this.prev < i.catchLoc) return handle(i.catchLoc, !0);
              } else {
                if (!u) throw Error("try statement without catch or finally");
                if (this.prev < i.finallyLoc) return handle(i.finallyLoc);
              }
            }
          }
        },
        abrupt: function abrupt(t, e) {
          for (var r = this.tryEntries.length - 1; r >= 0; --r) {
            var o = this.tryEntries[r];
            if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) {
              var i = o;
              break;
            }
          }
          i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null);
          var a = i ? i.completion : {};
          return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a);
        },
        complete: function complete(t, e) {
          if ("throw" === t.type) throw t.arg;
          return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y;
        },
        finish: function finish(t) {
          for (var e = this.tryEntries.length - 1; e >= 0; --e) {
            var r = this.tryEntries[e];
            if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y;
          }
        },
        "catch": function _catch(t) {
          for (var e = this.tryEntries.length - 1; e >= 0; --e) {
            var r = this.tryEntries[e];
            if (r.tryLoc === t) {
              var n = r.completion;
              if ("throw" === n.type) {
                var o = n.arg;
                resetTryEntry(r);
              }
              return o;
            }
          }
          throw Error("illegal catch attempt");
        },
        delegateYield: function delegateYield(e, r, n) {
          return this.delegate = {
            iterator: values(e),
            resultName: r,
            nextLoc: n
          }, "next" === this.method && (this.arg = t), y;
        }
      }, e;
    }
    module.exports = _regeneratorRuntime, module.exports.__esModule = true, module.exports["default"] = module.exports;
  });

  // TODO(Babel 8): Remove this file.

  var runtime = regeneratorRuntime$1();
  var regenerator = runtime;

  // Copied from https://github.com/facebook/regenerator/blob/main/packages/runtime/runtime.js#L736=
  try {
    regeneratorRuntime = runtime;
  } catch (accidentalStrictMode) {
    if ((typeof globalThis === "undefined" ? "undefined" : _typeof(globalThis)) === "object") {
      globalThis.regeneratorRuntime = runtime;
    } else {
      Function("r", "regeneratorRuntime = r")(runtime);
    }
  }

  var HgTemplateEngine = /*#__PURE__*/function () {
    function HgTemplateEngine(handlebars) {
      _classCallCheck(this, HgTemplateEngine);
      this.handlebars = handlebars;
    }
    return _createClass(HgTemplateEngine, [{
      key: "renderString",
      value: function renderString(tplString, data) {
        var templateFunc = this.handlebars.compile(tplString);
        var result = templateFunc(data);
        return result;
      }
    }, {
      key: "render",
      value: function render(tpl, data) {
        if (_typeof(tpl) === 'object') {
          var wrapper = document.createElement('div');
          wrapper.appendChild(tpl);
          var templateString = wrapper.outerHTML.toString();
          var rendered = this.renderString(templateString, data);
          var wrapper2 = document.createElement('div');
          wrapper2.insertAdjacentHTML('beforeend', rendered);
          return wrapper2.firstElementChild.innerHTML;
        } else {
          return this.renderString(tpl, data);
        }
      }
    }]);
  }();

  var BaseTemplateEngine = /*#__PURE__*/function () {
    function BaseTemplateEngine() {
      _classCallCheck(this, BaseTemplateEngine);
    }
    return _createClass(BaseTemplateEngine, [{
      key: "renderMustacheVars",
      value: function renderMustacheVars(el, map) {
        var wrapper = document.createElement('div');
        wrapper.appendChild(el);
        var template = wrapper.outerHTML.toString();
        for (var _i = 0, _Object$keys = Object.keys(map); _i < _Object$keys.length; _i++) {
          var key = _Object$keys[_i];
          template = template.replace(new RegExp('{{ ' + key + ' }}', 'g'), map[key]);
        }
        var wrapper2 = document.createElement('div');
        wrapper2.insertAdjacentHTML('beforeend', template);
        return wrapper2.firstElementChild.innerHTML;
      }
    }, {
      key: "renderString",
      value: function renderString(tplString, data) {
        var resultString = (' ' + tplString).slice(1);
        for (var _i2 = 0, _Object$keys2 = Object.keys(data); _i2 < _Object$keys2.length; _i2++) {
          var key = _Object$keys2[_i2];
          resultString = resultString.replace(new RegExp('{{ ' + key + ' }}', 'g'), data[key]);
        }
        return resultString;
      }
    }, {
      key: "render",
      value: function render(tpl, data) {
        if (_typeof(tpl) === 'object') {
          return this.renderMustacheVars(tpl, data);
        } else {
          return this.renderString(tpl, data);
        }
      }
    }]);
  }();

  function _createForOfIteratorHelper$5(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$5(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$5(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$5(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$5(r, a) : void 0; } }
  function _arrayLikeToArray$5(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  var ResLoader = /*#__PURE__*/function () {
    function ResLoader() {
      _classCallCheck(this, ResLoader);
    }
    return _createClass(ResLoader, null, [{
      key: "dynamicImportSupported",
      value: function dynamicImportSupported() {
        try {
          new Function('import("")');
          return true;
        } catch (err) {
          return false;
        }
      }
    }, {
      key: "isInIE",
      value: function isInIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
          return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }
        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
          var rv = ua.indexOf('rv:');
          return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
          return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }
        return false;
      }
    }, {
      key: "loadJson",
      value: function loadJson(url) {
        var _this = this;
        return new Promise(function (resolve, reject) {
          _newArrowCheck(this, _this);
          fetch(url).then(function (response) {
            var _this2 = this;
            response.json().then(function (json) {
              _newArrowCheck(this, _this2);
              resolve(json);
            }.bind(this));
          });
        }.bind(this));
      }
    }, {
      key: "loadClass",
      value: function loadClass(className, path, callback) {
        var _this3 = this;
        var nowindow = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
        var dynImportOff = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
        var loaded;
        var setToWin = function setToWin() {
          _newArrowCheck(this, _this3);
          if (!nowindow) {
            loaded.then(function (m) {
              if (m[className]) {
                window[className] = m[className];
                return m;
              }
            });
          }
        }.bind(this);
        var applyCallback = function applyCallback() {
          _newArrowCheck(this, _this3);
          if (callback) {
            loaded.then(callback);
          }
        }.bind(this);
        if (!dynImportOff && ResLoader.dynamicImportSupported() && !ResLoader.isInIE()) {
          loaded = require("".concat(path));
          setToWin();
          applyCallback();
          return loaded;
        } else {
          loaded = new Promise(function (resolve, reject) {
            var script = document.createElement('script');
            script.onload = resolve;
            script.onerror = reject;
            script.async = true;
            script.src = path;
            document.body.appendChild(script);
          }.bind(this));
          setToWin();
          applyCallback();
          return loaded;
        }
      }
    }, {
      key: "deepCopy",
      value: function deepCopy(from, to, level) {
        var maxLevel = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 256;
        if (from == null || _typeof(from) != "object") return from;
        if (from.constructor != Object && from.constructor != Array) return from;
        if (from.constructor == Date || from.constructor == RegExp || from.constructor == Function || from.constructor == String || from.constructor == Number || from.constructor == Boolean) return new from.constructor(from);
        to = to || new from.constructor();
        if (!level) level = 0;
        level++;
        if (level <= maxLevel) {
          for (var name in from) {
            to[name] = ResLoader.deepCopy(from[name], null);
          }
        }
        return to;
      }
    }, {
      key: "instIndex",
      value: function instIndex(className, path, factory) {
        return factory ? className + '::' + path + '::' + factory.toString().replace(/(\r\n|\n|\r| |\t)/gm, "") : className + '::' + path;
      }
    }, {
      key: "promiseStore",
      value: function promiseStore() {
        var resLoader = window.ResLoader ? window.ResLoader : ResLoader;
        if (!resLoader._loadPromises) {
          resLoader._loadPromises = new Map();
        }
        return resLoader._loadPromises;
      }
    }, {
      key: "instanceStore",
      value: function instanceStore() {
        var resLoader = window.ResLoader ? window.ResLoader : ResLoader;
        if (!resLoader._loadInstances) {
          resLoader._loadInstances = new Map();
        }
        return resLoader._loadInstances;
      }
    }, {
      key: "cacheClassDefs",
      value: function cacheClassDefs() {
        var _iterator = _createForOfIteratorHelper$5(arguments),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var def = _step.value;
            if (def.name && !window[def.name]) {
              window[def.name] = def;
            }
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      }

      /**
       * does class and instance loading.
       * If class with the same name and path was previously loaded it's previous instance will be returned 
       * by default and until forceNewInstance arg is set. 
       * 
       * className - class name string
       * path - loading path
       * factory - external method to create and setup new instance
       * forceNewInstance - will do new instance with provided argument factory or class constructor. 
       * keepState - link state props from prev instance
       * deepClone - recursive copy from prev instance
       * promiseStore - use external Map for loading promises cache
       * instanceStore - use external Map for class instance cache
       * cloneImpl - use external implementation for deepClone operation 
       * dynImmportOff - turn of dynamic import use (e.g. for transpilled code)
       * sync return instance without promise, class def must be preloaded and assigned to window
       * */
    }, {
      key: "dynLoad",
      value: function dynLoad(className, path, factory, forceNewInstance, keepState, deepClone, promiseStore, instanceStore, cloneImpl, dynImportOff, sync, stDef) {
        var _this4 = this;
        var index = ResLoader.instIndex(className, path, factory);
        var promises;
        var instances;
        var doNewInstance = function doNewInstance(model) {
          var instance;
          if (factory) {
            instance = factory(model.constructor);
          } else {
            instance = new model.constructor();
          }
          if (keepState) {
            if (deepClone) {
              if (cloneImpl && typeof cloneImpl === 'function') {
                instance = cloneImpl(model);
              } else {
                ResLoader.deepCopy(model, instance);
              }
            } else {
              Object.assign(instance, model);
            }
          }
          return instance;
        };
        if (!promiseStore) {
          promises = ResLoader.promiseStore();
        } else {
          promises = promiseStore;
        }
        if (!instanceStore) {
          instances = ResLoader.instanceStore();
        } else {
          instances = instanceStore;
        }
        if (instances.get(index)) {
          if (forceNewInstance) {
            var model = instances.get(index);
            var instance = doNewInstance(model);
            return sync ? instance : Promise.resolve(instance);
          } else {
            return sync ? instances.get(index) : Promise.resolve(instances.get(index));
          }
        } else {
          if (sync) {
            if (stDef && !window[className]) {
              window[className] = stDef;
            }
            var def = Function('return ' + className)();
            var _instance;
            if (factory) {
              _instance = factory(def);
            } else {
              _instance = new def();
            }
            instances.set(index, _instance);
            promises.set(index, Promise.resolve(_instance));
            return _instance;
          } else {
            if (!promises.get(index)) {
              promises.set(index, new Promise(function (resolve, reject) {
                var _this5 = this;
                _newArrowCheck(this, _this4);
                try {
                  if (stDef && !window[className]) {
                    window[className] = stDef;
                  }
                  var _def = Function('return ' + className)();
                  var _instance2;
                  if (factory) {
                    _instance2 = factory(_def);
                  } else {
                    _instance2 = new _def();
                  }
                  instances.set(index, _instance2);
                  resolve(_instance2);
                } catch (_unused) {
                  if (path.endsWith(".json")) {
                    promises.set(index, ResLoader.loadJson(path));
                    promises.get(index).then(function (m) {
                      _newArrowCheck(this, _this5);
                      resolve(m);
                    }.bind(this));
                  } else {
                    promises.set(index, ResLoader.loadClass(className, path, null, false, dynImportOff));
                    promises.get(index).then(function (m) {
                      _newArrowCheck(this, _this5);
                      var def = typeof m[className] === 'function' ? m[className] : window[className];
                      if (def) {
                        var _instance3;
                        if (factory) {
                          _instance3 = factory(def);
                        } else {
                          _instance3 = new def();
                        }
                        instances.set(index, _instance3);
                        resolve(_instance3);
                      } else {
                        reject("expected constructor for ".concat(className, " not found"));
                      }
                    }.bind(this));
                  }
                }
              }.bind(this)));
            }
            if (forceNewInstance) {
              return new Promise(function (resolve, reject) {
                var _this6 = this;
                _newArrowCheck(this, _this4);
                var onLoaded = promises.get(index);
                onLoaded.then(function (model) {
                  _newArrowCheck(this, _this6);
                  var instance = doNewInstance(model);
                  if (!instances.get(index)) {
                    instances.set(index, instance);
                  }
                  return resolve(instance);
                }.bind(this));
              }.bind(this));
            } else {
              return promises.get(index);
            }
          }
        }
      }
    }]);
  }();

  var RD_SHARED_ATTRS = {
    'rd-cache': 'cacheTemplates',
    'rd-cache-global': 'allowGlobalTemplates',
    'rd-var-render': 'variableRender',
    'rd-tpl-fmt': 'tplFmt',
    'basepath': 'basePath',
    'theme': 'theme'
  };
  var RD_OWN_ATTRS = {
    'rd-own': 'rdOwn',
    'rd-cache': 'cacheTemplates',
    'rd-cache-global': 'allowGlobalTemplates',
    'rd-var-render': 'variableRender',
    'rd-tpl-fmt': 'tplFmt'
  };
  var Renderer = /*#__PURE__*/function () {
    //templates = {};

    function Renderer() {
      _classCallCheck(this, Renderer);
      this.templates = {};
      this.templateLoad = new Map();
      this.translations = {};
      this.cacheTemplates = true;
      this.allowGlobalTemplates = true;
      this.variableRender = true;
      if (window['$'] !== undefined || window['jQuery'] !== undefined) {
        this.jquery = $ || jQuery.noConflict();
      }
    }
    return _createClass(Renderer, [{
      key: "confFromEl",
      value: function confFromEl(el) {
        for (var _i = 0, _Object$keys = Object.keys(RD_SHARED_ATTRS); _i < _Object$keys.length; _i++) {
          var attrName = _Object$keys[_i];
          var value = el.getAttribute(attrName);
          if (value !== null && value !== undefined) {
            try {
              this[RD_SHARED_ATTRS[attrName]] = JSON.parse(value);
            } catch (e) {
              this[RD_SHARED_ATTRS[attrName]] = value;
            }
          }
        }
      }
    }, {
      key: "templateEngine",
      get: function get() {
        if (!this._templateEngine) {
          if (this.variableRender === 'handlebars') {
            if (this.Handlebars !== undefined) {
              this._templateEngine = new HgTemplateEngine(this.Handlebars);
            } else {
              if (window && window['Handlebars']) {
                this._templateEngine = new HgTemplateEngine(window['Handlebars']);
              } else {
                console.error('no handlebars found in window or property');
              }
            }
          } else {
            if (typeof window[this.variableRender] === 'function') {
              window[this.variableRender].call(this);
            } else {
              this._templateEngine = new BaseTemplateEngine();
            }
          }
        }
        return this._templateEngine;
      }
    }, {
      key: "queryTemplate",
      value: function queryTemplate(sl) {
        if (this.templates[sl] !== null && this.templates[sl] !== undefined) {
          return this.templates[sl];
        } else {
          var template = document.querySelector(sl);
          if (template !== null && template !== undefined) {
            this.templates[sl] = template;
          } else {
            console.warn("Template with selector ".concat(sl, " not found"));
          }
          return this.templates[sl];
        }
      }
    }, {
      key: "loadTemplate",
      value: function loadTemplate(path) {
        var _this = this;
        return new Promise(function (resolve, reject) {
          _newArrowCheck(this, _this);
          fetch(path).then(function (response) {
            var _this2 = this;
            response.text().then(function (text) {
              _newArrowCheck(this, _this2);
              resolve(text);
            }.bind(this));
          });
        }.bind(this));
      }
    }, {
      key: "findTemplateEl",
      value: function findTemplateEl(id, hostEl) {
        var el = null;
        if (this.cacheTemplates) {
          if (hostEl) {
            el = hostEl.querySelector('#' + id);
            if (!el) {
              el = hostEl.querySelector(id);
            }
          }
          if (this.allowGlobalTemplates) {
            if (!el) {
              el = document.getElementById(id);
              if (el && el.parentElement && el.parentElement.tagName !== 'DIV' && el.parentElement.tagName !== 'BODY' && el.parentElement.tagName !== 'HEAD' && el.parentElement.tagName !== 'HTML') {
                // skip internal customElements overrides as they must not be shared
                el = null;
              }
              if (!el) {
                el = document.querySelector(id);
              }
            }
          }
        }
        return el;
      }
    }, {
      key: "genElPathString",
      value: function genElPathString(el) {
        var path = el.nodeName.split("-").join('__');
        var parent = el.parentNode;
        while (parent && parent.tagName) {
          path = parent.nodeName.split("-").join('__') + '_' + path;
          parent = parent.parentNode;
        }
        return path;
      }
    }, {
      key: "suggestId",
      value: function suggestId(id) {
        if (document.querySelector('#' + id) !== null) {
          var nums = id.match(/.*(\d+)/);
          if (nums !== null) {
            var num = nums[1];
            var name = id.replace(num, '');
            id = this.suggestId(name + (++num).toString());
          } else {
            id = this.suggestId(id + '2');
          }
        }
        return id;
      }
    }, {
      key: "genElId",
      value: function genElId(el) {
        var id = el.getAttribute('id');
        if (!id) {
          var domPath = this.genElPathString(el);
          id = this.suggestId(domPath);
        }
        return id;
      }
    }, {
      key: "mountTemplate",
      value: function mountTemplate(path, id, hostEl, preRenData) {
        var tplHash = path;
        if (hostEl && hostEl.constructor) {
          // different classes must not share the same template
          tplHash = hostEl.constructor.name + ":" + tplHash;
        }
        if (preRenData && !preRenData.constructor) {
          // pre-rendered values must not overwrite
          tplHash = tplHash + JSON.stringify(preRenData);
        }
        if (!this.templateLoad.has(tplHash)) {
          this.templateLoad.set(tplHash, new Promise(function (resolve, reject) {
            var _this3 = this;
            var el = this.findTemplateEl(id, hostEl);
            if (el !== null) {
              var elInstance = document.importNode(el, true);
              // can be mustache or native template inlined and overriden
              if (this.variableRender && this.templateEngine && preRenData) {
                elInstance.innerHTML = this.templateEngine.render(elInstance.innerHTML, preRenData);
              }
              resolve(elInstance);
            } else {
              this.loadTemplate(path).then(function (body) {
                _newArrowCheck(this, _this3);
                var template = this.findTemplateEl(id, hostEl);
                if (!template) {
                  if (this.tplFmt === 'handlebars') {
                    template = this.createEl('script');
                    template.setAttribute('type', 'text/x-handlebars-template');
                  } else {
                    template = this.createEl('template');
                  }
                }
                template.setAttribute('id', id);
                template.innerHTML = body;
                if (this.cacheTemplates) {
                  el = this.findTemplateEl(id, hostEl);
                  if (this.allowGlobalTemplates) {
                    if (template.hasAttribute('id') && !document.getElementById(template.getAttribute('id'))) {
                      document.body.appendChild(template);
                    }
                  } else {
                    hostEl.appendChild(template);
                  }
                  el = this.findTemplateEl(id, hostEl);
                }
                if (this.variableRender && this.templateEngine && preRenData) {
                  var contents = this.templateEngine.render(body, preRenData);
                  var templateCopy = document.importNode(template, true);
                  templateCopy.innerHTML = contents;
                  resolve(templateCopy);
                } else {
                  resolve(el);
                }
              }.bind(this));
            }
          }.bind(this)));
        }
        return this.templateLoad.get(tplHash) || Promise.reject('Template load error', tplHash);
      }
    }, {
      key: "prepareTemplate",
      value: function prepareTemplate(tpl) {
        if (this.tplFmt && this.tplFmt === 'handlebars') {
          var el = this.jquery(tpl.innerHTML);
          var html = '';
          if (el.length > 0) {
            html = this.jquery().prop ? this.jquery(tpl.innerHTML).prop('outerHTML') : this.jquery(tpl.innerHTML)[0].outerHTML;
          }
          var wrapper = this.createEl('div');
          this.jquery(wrapper).append(html);
          return wrapper.firstElementChild;
        } else {
          if (tpl.length) {
            return document.importNode(tpl[0].content, true);
          } else {
            return document.importNode(tpl.content, true);
          }
        }
      }
    }, {
      key: "renderTemplate",
      value: function renderTemplate(sl, dataMap) {
        var template = this.queryTemplate(sl);
        var fragment = document.importNode(template.content, true);
        for (var _i2 = 0, _Object$keys2 = Object.keys(dataMap); _i2 < _Object$keys2.length; _i2++) {
          var _sl = _Object$keys2[_i2];
          var entry = dataMap[_sl];
          var el = fragment.querySelector(_sl);
          if (el !== null) {
            if (entry.type === 'attr') {
              el.setAttribute(entry.attr, entry.value);
            } else {
              el.textContent = entry.value;
            }
          } else {
            console.warn("Element with selector ".concat(_sl, " not found in DOM"));
          }
        }
        return fragment;
      }
    }, {
      key: "renderMustacheVars",
      value: function renderMustacheVars(el, map) {
        var wrapper = this.createEl('div');
        if (typeof el === 'string') {
          wrapper.insertAdjacentHTML('beforeend', el);
        } else {
          wrapper.appendChild(el);
        }
        var template = wrapper.outerHTML.toString(); // :TODO detect if document-fragment or element is passed

        var rendered = this.replaceVars(template, map);
        var wrapper2 = this.createEl('div');
        wrapper2.insertAdjacentHTML('beforeend', rendered);
        return wrapper2.firstElementChild.innerHTML;
      }
    }, {
      key: "replaceVars",
      value: function replaceVars(target, map) {
        for (var _i3 = 0, _Object$keys3 = Object.keys(map); _i3 < _Object$keys3.length; _i3++) {
          var key = _Object$keys3[_i3];
          target = target.replace(new RegExp('{{ ' + key + ' }}', 'g'), map[key]);
        }
        return target;
      }
    }, {
      key: "createEl",
      value: function createEl(tagName, options) {
        return document.createElement(tagName, options);
      }
    }, {
      key: "renderWithTr",
      value: function renderWithTr(trJson, targetEl) {
        var _this4 = this;
        var fakeRoot = this.createEl('div');
        fakeRoot.appendChild(targetEl);
        fakeRoot.querySelectorAll('[tr]').forEach(function (el) {
          _newArrowCheck(this, _this4);
          var tr = el.getAttribute('tr');
          if (tr === 'contents') {
            var trValue = trJson[el.innerHTML];
            if (trValue !== undefined) {
              el.innerHTML = trValue;
            }
          } else if (tr.startsWith('attr')) {
            var trInfo = tr.split(':');
            var _trValue = trJson[trInfo[2]];
            if (_trValue !== undefined) {
              el.setAttribute(trInfo[1], _trValue);
            }
          }
        }.bind(this));
        return fakeRoot.firstChild.cloneNode(true);
      }
    }, {
      key: "localizeEl",
      value: function localizeEl(targetEl, locale, trUrl, trName) {
        var _this5 = this;
        var self = this;
        var promiseStore = ResLoader.promiseStore();
        ResLoader.dynLoad(trName, trUrl);
        return new Promise(function (resolve, reject) {
          var _this6 = this;
          _newArrowCheck(this, _this5);
          var index = ResLoader.instIndex(trName, trUrl);
          promiseStore.get(index).then(function (json) {
            _newArrowCheck(this, _this6);
            self.translations[locale] = Object.assign(self.translations[locale] ? self.translations[locale] : {}, json);
            resolve(self.renderWithTr(self.translations[locale], targetEl));
          }.bind(this));
        }.bind(this));
      }
    }, {
      key: "whenElRendered",
      value: function whenElRendered(el, callback) {
        return Renderer.callWhenRendered(el, callback);
      }
    }], [{
      key: "hasRenderAttrs",
      value: function hasRenderAttrs(el) {
        for (var _i4 = 0, _Object$keys4 = Object.keys(RD_SHARED_ATTRS); _i4 < _Object$keys4.length; _i4++) {
          var attrName = _Object$keys4[_i4];
          var value = el.getAttribute(attrName);
          if (value !== null && value !== undefined) {
            return true;
          }
        }
        return false;
      }
    }, {
      key: "hasOwnAttrs",
      value: function hasOwnAttrs(el) {
        for (var _i5 = 0, _Object$keys5 = Object.keys(RD_OWN_ATTRS); _i5 < _Object$keys5.length; _i5++) {
          var attrName = _Object$keys5[_i5];
          var value = el.getAttribute(attrName);
          if (value !== null && value !== undefined) {
            return true;
          }
        }
        return false;
      }
    }, {
      key: "configureForElement",
      value: function configureForElement(el) {
        if (!el.renderer) {
          el.renderer = new Renderer();
          el.renderer.confFromEl(el);
        } else {
          // if one of renderer options specified element needs own extended renderer
          if (Renderer.hasOwnAttrs(el)) {
            var rendererCopy = el.renderer;
            el.renderer = new Renderer();
            for (var _i6 = 0, _Object$keys6 = Object.keys(RD_SHARED_ATTRS); _i6 < _Object$keys6.length; _i6++) {
              var attrName = _Object$keys6[_i6];
              if (rendererCopy[RD_SHARED_ATTRS[attrName]] !== null && rendererCopy[RD_SHARED_ATTRS[attrName]] !== undefined) {
                el.renderer[RD_SHARED_ATTRS[attrName]] = rendererCopy[RD_SHARED_ATTRS[attrName]];
              }
            }
            el.renderer.confFromEl(el);
          }
        }
      }
    }, {
      key: "callOnceOn",
      value: function callOnceOn(el, eventName, callback) {
        var _this7 = this;
        return new Promise(function (resolve, reject) {
          _newArrowCheck(this, _this7);
          if (el.impl || el._implPromise) {
            if (!el.impl || el.impl && !el.implRenderTimest) {
              el.addEventListener(eventName, function skElementWhenEvt(event) {
                if (event.target === el) {
                  el.removeEventListener(eventName, skElementWhenEvt);
                  resolve(callback ? callback(event) : null);
                }
              });
            } else {
              resolve(callback ? callback() : null);
            }
          } else {
            resolve(callback ? callback() : null);
          }
        }.bind(this));
      }
    }, {
      key: "callWhenRendered",
      value: function callWhenRendered(el, callback) {
        return Renderer.callOnceOn(el, 'skrender', callback);
      }
    }]);
  }();

  var SkTheme = /*#__PURE__*/_createClass(function SkTheme(configEl) {
    _classCallCheck(this, SkTheme);
    this.configEl = configEl;
  });

  function _callSuper$6(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$6() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$6() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$6 = function _isNativeReflectConstruct() { return !!t; })(); }
  var DefaultTheme = /*#__PURE__*/function (_SkTheme) {
    function DefaultTheme() {
      _classCallCheck(this, DefaultTheme);
      return _callSuper$6(this, DefaultTheme, arguments);
    }
    _inherits(DefaultTheme, _SkTheme);
    return _createClass(DefaultTheme, [{
      key: "basePath",
      get: function get() {
        if (!this._basePath) {
          this._basePath = this.configEl ? "".concat(this.configEl.basePath, "/theme/default") : '/node_modules/skinny-widgets/theme/default';
        }
        return this._basePath;
      }
    }, {
      key: "styles",
      get: function get() {
        if (!this._styles) {
          this._styles = {
            'default.css': "".concat(this.basePath, "/default.css")
          };
        }
        return this._styles;
      }
    }]);
  }(SkTheme);

  var SkThemes = /*#__PURE__*/function () {
    function SkThemes() {
      _classCallCheck(this, SkThemes);
    }
    return _createClass(SkThemes, null, [{
      key: "capitalizeFirstLetter",
      value: function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
      }
    }, {
      key: "byName",
      value: function byName(name, config) {
        if (name && name !== 'default') {
          var themePath = '/node_modules/sk-theme-' + name + '/src/' + name + '-theme.js';
          var className = SkThemes.capitalizeFirstLetter(name) + 'Theme';
          var dynImportOff = config.hasAttribute('dimport') && config.getAttribute('dimport') === 'false';
          if (dynImportOff) {
            return ResLoader.loadClass(className, themePath, null, false, true);
          } else {
            return ResLoader.loadClass(className, themePath);
          }
        } else {
          return Promise.resolve(new DefaultTheme(config));
        }
      }
    }]);
  }();

  function _createForOfIteratorHelper$4(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$4(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$4(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$4(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$4(r, a) : void 0; } }
  function _arrayLikeToArray$4(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  var LOG_LEVEL_DEFAULT = 'info,error,warn';
  var ConsoleLogger = /*#__PURE__*/function () {
    function ConsoleLogger() {
      _classCallCheck(this, ConsoleLogger);
    }
    return _createClass(ConsoleLogger, [{
      key: "levels",
      get: function get() {
        return this._levels;
      },
      set: function set(levels) {
        this._logLevel = levels;
        this._levels = levels.split(",");
      }
    }, {
      key: "logLevel",
      get: function get() {
        return this._logLevel || LOG_LEVEL_DEFAULT;
      },
      set: function set(level) {
        this._logLevel = level;
        this._levels = level.split(",");
      }
    }, {
      key: "info",
      value: function info() {
        if (this.levels.indexOf('info') > -1) {
          if (typeof console !== 'undefined') {
            console.log.apply(console, arguments);
          }
        }
      }
    }, {
      key: "debug",
      value: function debug() {
        if (this.levels.indexOf('debug') > -1) {
          if (typeof console !== 'undefined') {
            console.debug.apply(console, arguments);
          }
        }
      }
    }, {
      key: "error",
      value: function error() {
        if (this.levels.indexOf('error') > -1) {
          if (typeof console !== 'undefined') {
            console.error.apply(console, arguments);
          }
        }
      }
    }, {
      key: "warn",
      value: function warn() {
        if (this.levels.indexOf('warn') > -1) {
          if (typeof console !== 'undefined') {
            console.warn.apply(console, arguments);
          }
        }
      }
    }], [{
      key: "instance",
      value: function instance() {
        if (!ConsoleLogger._instance) {
          ConsoleLogger._instance = new ConsoleLogger();
          ConsoleLogger._instance.levels = LOG_LEVEL_DEFAULT;
        }
        return ConsoleLogger._instance;
      }
    }, {
      key: "info",
      value: function info() {
        ConsoleLogger.instance().info(arguments);
      }
    }, {
      key: "debug",
      value: function debug() {
        ConsoleLogger.instance().debug(arguments);
      }
    }, {
      key: "error",
      value: function error() {
        ConsoleLogger.instance().error(arguments);
      }
    }, {
      key: "warn",
      value: function warn() {
        ConsoleLogger.instance().warn(arguments);
      }
    }]);
  }();
  var MemoryLogger = /*#__PURE__*/function () {
    function MemoryLogger() {
      _classCallCheck(this, MemoryLogger);
    }
    return _createClass(MemoryLogger, [{
      key: "logs",
      get: function get() {
        if (!this._logs) {
          this._logs = [];
        }
        return this._logs;
      },
      set: function set(logs) {
        this._logs = logs;
      }
    }, {
      key: "levels",
      get: function get() {
        return this._levels;
      },
      set: function set(levels) {
        this._logLevel = levels;
        this._levels = levels.split(",");
      }
    }, {
      key: "logLevel",
      get: function get() {
        return this._logLevel || LOG_LEVEL_DEFAULT;
      },
      set: function set(level) {
        this._logLevel = level;
        this._levels = level.split(",");
      }
    }, {
      key: "info",
      value: function info() {
        if (this.levels.indexOf('info') > -1) {
          this.logs.push({
            time: Date.now(),
            message: arguments
          });
        }
      }
    }, {
      key: "debug",
      value: function debug() {
        if (this.levels.indexOf('debug') > -1) {
          this.logs.push({
            time: Date.now(),
            message: arguments
          });
        }
      }
    }, {
      key: "error",
      value: function error() {
        if (this.levels.indexOf('error') > -1) {
          this.logs.push({
            time: Date.now(),
            message: arguments
          });
        }
      }
    }, {
      key: "warn",
      value: function warn() {
        if (this.levels.indexOf('warn') > -1) {
          this.logs.push({
            time: Date.now(),
            message: arguments
          });
        }
      }
    }], [{
      key: "instance",
      value: function instance() {
        if (!MemoryLogger._instance) {
          MemoryLogger._instance = new MemoryLogger();
          MemoryLogger._instance.levels = LOG_LEVEL_DEFAULT;
        }
        return MemoryLogger._instance;
      }
    }, {
      key: "info",
      value: function info() {
        MemoryLogger.instance().info(arguments);
      }
    }, {
      key: "debug",
      value: function debug() {
        MemoryLogger.instance().debug(arguments);
      }
    }, {
      key: "error",
      value: function error() {
        MemoryLogger.instance().error(arguments);
      }
    }, {
      key: "warn",
      value: function warn() {
        MemoryLogger.instance().warn(arguments);
      }
    }]);
  }();
  var SESSION_LOGS_ATTR = "skLogs";
  var SessionLogger = /*#__PURE__*/function () {
    function SessionLogger() {
      _classCallCheck(this, SessionLogger);
    }
    return _createClass(SessionLogger, [{
      key: "logs",
      get: function get() {
        if (!this._logs) {
          try {
            var prevLogs = sessionStorage.get(SESSION_LOGS_ATTR) || "[]";
            if (prevLogs) {
              var logsParsed = JSON.parse(prevLogs);
              if (Array.isArray(logsParsed) && logsParsed.length > -1) {
                this._logs = logsParsed;
              } else {
                this._logs = [];
              }
            } else {
              this._logs = [];
            }
          } catch (_unused) {
            this._logs = [];
          }
        }
        return this._logs;
      },
      set: function set(logs) {
        this._logs = logs;
      }
    }, {
      key: "levels",
      get: function get() {
        return this._levels;
      },
      set: function set(levels) {
        this._logLevel = levels;
        this._levels = levels.split(",");
      }
    }, {
      key: "logLevel",
      get: function get() {
        return this._logLevel || LOG_LEVEL_DEFAULT;
      },
      set: function set(level) {
        this._logLevel = level;
        this._levels = level.split(",");
      }
    }, {
      key: "writeLog",
      value: function writeLog() {
        var message = [];
        var _iterator = _createForOfIteratorHelper$4(arguments),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var arg = _step.value;
            if (typeof arg === "string" || typeof arg === "number") {
              message.push(arg.toString());
            } else if (_typeof(arg) === "object") {
              var subArg = {};
              var _iterator2 = _createForOfIteratorHelper$4(Object.getOwnPropertyNames(arg)),
                _step2;
              try {
                for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                  var subArgName = _step2.value;
                  if (["caller", "callee", "arguments", "length"].indexOf(subArgName) < 0) {
                    if (typeof arg[subArgName] === "string" || typeof arg[subArgName] === "number") {
                      subArg[subArgName] = arg[subArgName];
                    }
                  }
                }
              } catch (err) {
                _iterator2.e(err);
              } finally {
                _iterator2.f();
              }
              try {
                if (Object.getOwnPropertyNames(subArg).length > -1) {
                  message.push(JSON.stringify(subArg));
                }
              } catch (_unused2) {}
            }
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
        if (message.length > -1) {
          this.logs.push({
            time: Date.now(),
            message: message
          });
          sessionStorage.setItem(SESSION_LOGS_ATTR, JSON.stringify(this.logs));
        }
      }
    }, {
      key: "info",
      value: function info() {
        if (this.levels.indexOf('info') > -1) {
          this.writeLog(arguments);
        }
      }
    }, {
      key: "debug",
      value: function debug() {
        if (this.levels.indexOf('debug') > -1) {
          this.writeLog(arguments);
        }
      }
    }, {
      key: "error",
      value: function error() {
        if (this.levels.indexOf('error') > -1) {
          this.writeLog(arguments);
        }
      }
    }, {
      key: "warn",
      value: function warn() {
        if (this.levels.indexOf('warn') > -1) {
          this.writeLog(arguments);
        }
      }
    }], [{
      key: "instance",
      value: function instance() {
        if (!SessionLogger._instance) {
          SessionLogger._instance = new SessionLogger();
          SessionLogger._instance.levels = LOG_LEVEL_DEFAULT;
        }
        return SessionLogger._instance;
      }
    }, {
      key: "info",
      value: function info() {
        SessionLogger.instance().info(arguments);
      }
    }, {
      key: "debug",
      value: function debug() {
        SessionLogger.instance().debug(arguments);
      }
    }, {
      key: "error",
      value: function error() {
        SessionLogger.instance().error(arguments);
      }
    }, {
      key: "warn",
      value: function warn() {
        SessionLogger.instance().warn(arguments);
      }
    }]);
  }();

  var EL_SHARED_ATTRS = {
    'theme': 'theme',
    'base-path': 'basePath',
    'use-shadow-root': 'useShadowRoot',
    'lang': 'lang',
    'reflective': 'reflective',
    'gen-ids': 'genIds',
    'st-store': 'stateStoreBackend',
    'styles': 'themeStyles',
    'log-lv': 'logLevel',
    'tpl-vars': 'tplVars'
  };
  var SK_CONFIG_TN = "sk-config";
  var DIMPORTS_AN = "dimports";

  function _defineProperty(e, r, t) {
    return (r = toPropertyKey(r)) in e ? Object.defineProperty(e, r, {
      value: t,
      enumerable: !0,
      configurable: !0,
      writable: !0
    }) : e[r] = t, e;
  }

  var EventTarget = function EventTarget() {
    this.listeners = {};
  };
  EventTarget.prototype.listeners = null;
  EventTarget.prototype.addEventListener = function (type, callback) {
    if (!(type in this.listeners)) {
      this.listeners[type] = [];
    }
    this.listeners[type].push(callback);
  };
  EventTarget.prototype.removeEventListener = function (type, callback) {
    if (!(type in this.listeners)) {
      return;
    }
    var stack = this.listeners[type];
    for (var i = 0, l = stack.length; i < l; i++) {
      if (stack[i] === callback) {
        stack.splice(i, 1);
        return;
      }
    }
  };
  EventTarget.prototype.dispatchEvent = function (event) {
    if (!(event.type in this.listeners)) {
      return true;
    }
    var stack = this.listeners[event.type].slice();
    for (var i = 0, l = stack.length; i < l; i++) {
      stack[i].call(this, event);
    }
    return !event.defaultPrevented;
  };

  function _callSuper$5(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$5() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$5() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$5 = function _isNativeReflectConstruct() { return !!t; })(); }
  var SK_RENDER_EVT = 'skrender';
  var SkRenderEvent = /*#__PURE__*/function (_CustomEvent) {
    function SkRenderEvent(options) {
      _classCallCheck(this, SkRenderEvent);
      return _callSuper$5(this, SkRenderEvent, [SK_RENDER_EVT, options]);
    }
    _inherits(SkRenderEvent, _CustomEvent);
    return _createClass(SkRenderEvent);
  }(/*#__PURE__*/_wrapNativeSuper(CustomEvent));

  function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
  function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
  function _createForOfIteratorHelper$3(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$3(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$3(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$3(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$3(r, a) : void 0; } }
  function _arrayLikeToArray$3(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper$4(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$4() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$4() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$4 = function _isNativeReflectConstruct() { return !!t; })(); }

  /**
   * elements than can have action attribute to be binded with event or callback, currently in sk-dialog and sk-form only
   * @type {string}
   */
  var ACTION_SUBELEMENTS_SL = 'button,sk-button';
  var SkComponentImpl = /*#__PURE__*/function (_EventTarget) {
    function SkComponentImpl(comp) {
      var _this;
      _classCallCheck(this, SkComponentImpl);
      _this = _callSuper$4(this, SkComponentImpl);
      _this.comp = comp;
      return _this;
    }
    _inherits(SkComponentImpl, _EventTarget);
    return _createClass(SkComponentImpl, [{
      key: "tplPath",
      get:
      /*    get tplPath() {
              if (! this._tplPath) {
                  this._tplPath = this.comp.tplPath || `${this.themePath}/${this.prefix}-sk-${this.suffix}.tpl.html`;
              }
              return this._tplPath;
          }*/

      function get() {
        if (!this._tplPath) {
          if (this.comp.tplPath) {
            this._tplPath = this.comp.tplPath;
          } else {
            this._tplPath = this.comp.configEl && this.comp.configEl.hasAttribute('tpl-path') ? "".concat(this.comp.configEl.getAttribute('tpl-path'), "/").concat(this.prefix, "-sk-").concat(this.suffix, ".tpl.html") : "/node_modules/sk-".concat(this.comp.cnSuffix, "-").concat(this.prefix, "/src/").concat(this.prefix, "-sk-").concat(this.suffix, ".tpl.html");
          }
        }
        return this._tplPath;
      },
      set: function set(tplPath) {
        this.comp.setAttribute('tpl-path', tplPath);
        this.tplPath;
      }
    }, {
      key: "themePath",
      get: function get() {
        if (this.skTheme && this.skTheme.basePath) {
          return this.skTheme.basePath;
        } else {
          var basePath = this.comp.basePath.toString();
          this.logger.warn('theme path is computed from basePath', basePath, this.skTheme);
          if (basePath.endsWith('/')) {
            // remove traling slash
            basePath = basePath.substr(0, basePath.length - 1);
          }
          return "".concat(basePath, "/theme/").concat(this.comp.theme);
        }
      }
    }, {
      key: "mountedStyles",
      get: function get() {
        if (!this._mountedStyles) {
          this._mountedStyles = {};
        }
        return this._mountedStyles;
      },
      set: function set(mountedStyles) {
        this._mountedStyles = mountedStyles;
      }
    }, {
      key: "bindEvents",
      value: function bindEvents() {}
    }, {
      key: "unbindEvents",
      value: function unbindEvents() {}
    }, {
      key: "dumpState",
      value: function dumpState() {}
    }, {
      key: "saveState",
      value: function saveState() {
        if (!this.comp.contentsState) {
          if (this.comp.useShadowRoot) {
            this.comp.contentsState = this.comp.el.host.innerHTML;
            this.comp.el.host.innerHTML = '';
          } else {
            this.comp.contentsState = this.comp.el.innerHTML;
            this.comp.el.innerHTML = '';
          }
        }
      }
    }, {
      key: "restoreState",
      value: function restoreState(state) {}
    }, {
      key: "enable",
      value: function enable() {}
    }, {
      key: "disable",
      value: function disable() {}
    }, {
      key: "afterRendered",
      value: function afterRendered() {
        this.comp.dispatchEvent(new CustomEvent('skafterrendered'));
      }
    }, {
      key: "beforeRendered",
      value: function beforeRendered() {
        this.comp.dispatchEvent(new CustomEvent('skbeforerendered'));
      }
    }, {
      key: "unmountStyles",
      value: function unmountStyles() {}
    }, {
      key: "logger",
      get: function get() {
        return this.comp.logger;
      }
    }, {
      key: "findStyle",
      value: function findStyle(seek) {
        var links = document.querySelectorAll('link');
        var matched = [];
        var _iterator = _createForOfIteratorHelper$3(links),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var link = _step.value;
            if (Array.isArray(seek)) {
              var _iterator2 = _createForOfIteratorHelper$3(seek),
                _step2;
              try {
                for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                  var seekItem = _step2.value;
                  if (link.href.match(seekItem)) {
                    matched.push(link);
                    break;
                  }
                }
              } catch (err) {
                _iterator2.e(err);
              } finally {
                _iterator2.f();
              }
            } else {
              if (link.href.match(seek)) {
                matched.push(link);
              }
            }
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
        return matched;
      }
    }, {
      key: "attachStyleByName",
      value: function attachStyleByName(name, target) {
        if (!target) {
          target = this.comp.el;
        }
        var path = this.comp.configEl.styles[name];
        if (path) {
          var link = document.createElement('link');
          link.setAttribute('rel', 'stylesheet');
          link.setAttribute('href', path);
          target.appendChild(link);
        } else {
          this.logger.warn('style not found with config el', name);
        }
      }
    }, {
      key: "attachStyleByPath",
      value: function attachStyleByPath(path, target) {
        if (!target) {
          target = this.comp.el;
        }
        if (target.querySelector("link[href='".concat(path, "']")) == null) {
          var link = document.createElement('link');
          link.setAttribute('rel', 'stylesheet');
          link.setAttribute('href', path);
          target.appendChild(link);
        }
      }
    }, {
      key: "genStyleLinks",
      value: function genStyleLinks() {
        if (this.comp.hasAttribute('styles') && this.comp.getAttribute('styles') === 'false') {
          return false;
        }
        if (this.skTheme && this.skTheme.styles) {
          var styles = this.skTheme.styles;
          var configStyles = this.comp.configEl && this.comp.configEl.styles ? Object.keys(this.comp.configEl.styles) : [];
          for (var _i = 0, _Object$keys = Object.keys(styles); _i < _Object$keys.length; _i++) {
            var styleName = _Object$keys[_i];
            var href = configStyles.includes(styleName) ? this.comp.configEl.styles[styleName] : styles[styleName];
            var fileName = this.fileNameFromUrl(href);
            if (!this.mountedStyles[fileName]) {
              // mount only not mounted before
              var link = document.createElement('link');
              link.setAttribute('rel', 'stylesheet');
              link.setAttribute('href', href);
              this.comp.el.appendChild(link);
              this.mountedStyles[fileName] = link;
              this.logger.info("mounted style: ".concat(fileName, " as ").concat(href, " to component: ").concat(this.comp.tagName));
            }
          }
        }
      }
    }, {
      key: "mountStyles",
      value: function mountStyles() {
        if (!this.skTheme && this.comp.skThemeLoader) {
          // :TODO optimize this
          this.comp.skThemeLoader.then(function (skTheme) {
            this.genStyleLinks();
          }.bind(this));
        } else {
          this.genStyleLinks();
        }
      }
    }, {
      key: "mountStyle",
      value: function mountStyle(fileNames, target) {
        if (!target) {
          target = this.comp.el;
        }
        if (this.comp.useShadowRoot) {
          if (this.comp.configEl && this.comp.configEl.styles) {
            if (Array.isArray(fileNames)) {
              var _iterator3 = _createForOfIteratorHelper$3(fileNames),
                _step3;
              try {
                for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
                  var name = _step3.value;
                  this.attachStyleByName(name, target);
                }
              } catch (err) {
                _iterator3.e(err);
              } finally {
                _iterator3.f();
              }
            } else {
              this.attachStyleByName(fileNames, target);
            }
          } else {
            var styles = this.findStyle(fileNames);
            if (styles && styles.length > 0) {
              var style = styles[0];
              if (style) {
                target.appendChild(document.importNode(style));
              } else {
                this.logger.warn('style not found', fileNames);
              }
            }
          }
        }
      }
    }, {
      key: "clearTplCache",
      value: function clearTplCache() {
        if (this.cachedTplId) {
          var tplEl = document.querySelector('#' + this.cachedTplId);
          if (tplEl !== null) {
            tplEl.remove();
          }
        }
      }
    }, {
      key: "cachedTplId",
      get: function get() {
        return this.comp.constructor.name + 'Tpl';
      }
    }, {
      key: "skFormParent",
      get: function get() {
        if (this._skFormParent === undefined) {
          this._skFormParent = this.findParent(this.comp, 'SK-FORM');
        }
        return this._skFormParent;
      }
    }, {
      key: "idGenEnabled",
      value: function idGenEnabled() {
        return this.comp.configEl && this.comp.configEl.hasAttribute('gen-ids') && this.comp.configEl.hasAttribute('gen-ids') !== 'false';
      }
    }, {
      key: "isInIE",
      value: function isInIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
          return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }
        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
          var rv = ua.indexOf('rv:');
          return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
          return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }
        return false;
      }
    }, {
      key: "initTheme",
      value: function initTheme() {
        if (!this.skTheme) {
          try {
            var cn = this.comp.capitalizeFirstLetter(this.comp.theme) + 'Theme';
            var def = Function('return ' + cn)();
            if (def) {
              this.skTheme = new def(this.comp.configEl);
              if (this.comp.configEl) {
                this.comp.configEl.skTheme = this.skTheme;
              }
              return Promise.resolve(this.skTheme);
            }
          } catch (_unused) {
            return new Promise(function (resolve) {
              this.comp.skThemeLoader.then(function (skTheme) {
                if (this.comp.configEl && this.comp.configEl.skTheme) {
                  this.skTheme = this.comp.configEl.skTheme;
                  resolve(this.skTheme);
                } else {
                  var _cn = this.comp.capitalizeFirstLetter(this.comp.theme) + 'Theme';
                  var _def = false;
                  if (skTheme[_cn]) {
                    _def = skTheme[_cn];
                  } else {
                    try {
                      _def = Function('return ' + _cn)();
                    } catch (_unused2) {
                      this.logger.warn('unable to load theme class');
                    }
                  }
                  if (_def) {
                    this.skTheme = new _def(this.comp.configEl);
                    if (this.comp.configEl) {
                      this.comp.configEl.skTheme = this.skTheme;
                    }
                  }
                  resolve(this.skTheme);
                }
              }.bind(this));
            }.bind(this));
          }
        } else {
          return Promise.resolve(this.skTheme);
        }
      }
    }, {
      key: "doRender",
      value: function doRender() {
        var id = this.getOrGenId();
        this.beforeRendered();
        this.renderWithVars(id);
        this.indexMountedStyles();
        this.afterRendered();
        this.bindEvents();
        this.comp.bindAutoRender();
        this.comp.setupConnections();
        if (this.comp.configEl && this.comp.skConfigElPolicy() === SK_CONFIG_EL_INDIVIDUAL) {
          this.comp.el.appendChild(this.comp.configEl);
        }
        this.comp.callPluginHook('onRenderEnd');
        this.comp.implRenderTimest = Date.now();
        this.comp.removeFromRendering();
        this.comp.dispatchEvent(new CustomEvent('rendered', {
          bubbles: true,
          composed: true
        })); // :DEPRECATED
        this.comp.dispatchEvent(new SkRenderEvent({
          bubbles: true,
          composed: true
        }));
        if (this.comp.renderDeferred) {
          this.comp.renderDeferred.resolve(this.comp);
        }
      }
    }, {
      key: "noTplRendering",
      value: function noTplRendering() {
        if (!this.tplPath) {
          return true;
        }
        var undef = this.tplPath.match('undefined');
        if (undef !== null && undef.length > 0) {
          return true;
        }
        return false;
      }
    }, {
      key: "renderImpl",
      value: function renderImpl() {
        var _this2 = this;
        if (this.noTplRendering()) {
          this.doRender();
        } else {
          var themeLoaded = this.initTheme();
          themeLoaded["finally"](function () {
            var _this3 = this;
            _newArrowCheck(this, _this2);
            // we just need to try loading styles
            this.comp.tpl = this.comp.renderer.findTemplateEl(this.cachedTplId, this.comp); // try to find overriden template
            if (!this.comp.tpl) {
              this.comp.renderer.mountTemplate(this.tplPath, this.cachedTplId, this.comp, {
                themePath: this.themePath
              }).then(function (tpl) {
                _newArrowCheck(this, _this3);
                this.comp.tpl = tpl;
                this.doRender();
              }.bind(this));
            } else {
              this.doRender();
            }
          }.bind(this));
        }
      }
    }, {
      key: "getOrGenId",
      value: function getOrGenId() {
        var id;
        if (!this.comp.getAttribute('id') && this.idGenEnabled()) {
          id = this.comp.renderer.genElId(this.comp);
          this.comp.setAttribute('id', id);
        } else {
          id = this.comp.getAttribute('id');
        }
        return id;
      }
    }, {
      key: "tplVars",
      get: function get() {
        if (!this._tplVars) {
          this._tplVars = this.comp.tplVars ? this.comp.tplVars : {};
        }
        return this._tplVars;
      },
      set: function set(tplVars) {
        this._tplVars = tplVars;
      }
    }, {
      key: "i18n",
      get: function get() {
        if (!this._i18n) {
          this._i18n = this.comp._i18n ? this.comp._i18n : {};
        }
        return this._i18n;
      },
      set: function set(i18n) {
        this._i18n = i18n;
      }
    }, {
      key: "renderWithVars",
      value: function renderWithVars(id, targetEl, tpl) {
        tpl = tpl || this.comp.tpl;
        if (!tpl) {
          return false;
        }
        targetEl = targetEl || this.comp.el;
        var el = this.comp.renderer.prepareTemplate(tpl);
        var rendered;
        if (this.comp.renderer.variableRender && this.comp.renderer.variableRender !== 'false') {
          rendered = this.comp.renderer.renderMustacheVars(el, _objectSpread(_objectSpread({
            id: id,
            themePath: this.themePath
          }, this.tplVars), this.i18n));
          targetEl.innerHTML = '';
          targetEl.innerHTML = rendered;
        } else {
          targetEl.innerHTML = '';
          targetEl.appendChild(el);
        }
      }
    }, {
      key: "fileNameFromUrl",
      value: function fileNameFromUrl(url) {
        return url ? url.substring(url.lastIndexOf('/') + 1) : null;
      }
    }, {
      key: "indexMountedStyles",
      value: function indexMountedStyles() {
        if (this.comp.hasAttribute('styles') && this.comp.getAttribute('styles') === 'false') {
          return false;
        }
        var links = this.comp.el.querySelectorAll('link');
        this.mountedStyles = {};
        var _iterator4 = _createForOfIteratorHelper$3(links),
          _step4;
        try {
          for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
            var link = _step4.value;
            var fileName = this.fileNameFromUrl(link.getAttribute('href'));
            this.mountedStyles[fileName] = link;
          }
        } catch (err) {
          _iterator4.e(err);
        } finally {
          _iterator4.f();
        }
        var styles = this.comp.el.querySelectorAll('style');
        var _iterator5 = _createForOfIteratorHelper$3(styles),
          _step5;
        try {
          for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
            var style = _step5.value;
            var _fileName = style.hasAttribute('data-file-name') ? style.getAttribute('data-file-name') : null;
            if (_fileName) {
              this.mountedStyles[_fileName] = style;
            }
          }
        } catch (err) {
          _iterator5.e(err);
        } finally {
          _iterator5.f();
        }
      }
    }, {
      key: "findParent",
      value: function findParent(el, tag) {
        while (el.parentNode) {
          el = el.parentNode;
          if (el.tagName === tag) return el;
        }
        return null;
      }
    }, {
      key: "validateWithAttrs",
      value: function validateWithAttrs() {
        if (this.skFormParent) {
          // if has sk-form parents, rely on it
          return true;
        }
        this.hasOwnValidator = false;
        var validatorNames = this.skValidators ? Object.keys(this.skValidators) : [];
        var _iterator6 = _createForOfIteratorHelper$3(validatorNames),
          _step6;
        try {
          for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
            var validatorName = _step6.value;
            // :TODO check there is no parent sk-form first
            if (this.comp.hasAttribute(validatorName)) {
              this.hasOwnValidator = true;
              var validator = this.skValidators[validatorName];
              var result = validator.validate(this.comp);
              return result;
            }
          }
        } catch (err) {
          _iterator6.e(err);
        } finally {
          _iterator6.f();
        }
        if (!this.hasOwnValidator) {
          // any value is valid if no validator defined
          return true;
        }
        return false;
      }
    }, {
      key: "showInvalid",
      value: function showInvalid() {
        if (this.comp.hasAttribute('validation-label') && this.comp.getAttribute('validation-label') !== 'disabled') {
          this.renderValidation();
        }
      }
    }, {
      key: "showValid",
      value: function showValid() {
        if (this.comp.hasAttribute('validation-label') && this.comp.getAttribute('validation-label') !== 'disabled') {
          this.flushValidation();
        }
      }
    }, {
      key: "renderValidation",
      value: function renderValidation(validity) {
        if (this.comp.hasAttribute('validation-label') && this.comp.getAttribute('validation-label') !== 'disabled') {
          var msgEl = this.comp.el.querySelector('.form-validation-message');
          if (!msgEl) {
            msgEl = this.comp.renderer.createEl('span');
            msgEl.classList.add('form-validation-message');
            msgEl.insertAdjacentHTML('afterbegin', validity || this.comp.validationMessage);
            this.comp.el.appendChild(msgEl);
          } else {
            msgEl.innerHTML = '';
            msgEl.insertAdjacentHTML('afterbegin', validity || this.comp.validationMessage);
          }
        }
      }
    }, {
      key: "flushValidation",
      value: function flushValidation() {
        var msgEl = this.comp.el.querySelector('.form-validation-message');
        if (msgEl) {
          if (msgEl.parentElement) {
            msgEl.parentElement.removeChild(msgEl);
          } else {
            msgEl.parentNode.removeChild(msgEl);
          }
        }
      }
    }, {
      key: "getScrollTop",
      value: function getScrollTop() {
        if (typeof pageYOffset != 'undefined') {
          return pageYOffset;
        } else {
          var body = _document.body;
          var _document = _document.documentElement;
          _document = _document.clientHeight ? _document : body;
          return _document.scrollTop;
        }
      }
    }, {
      key: "actionSubElementsSl",
      get: function get() {
        if (!this._actionSubElementsSl) {
          this._actionSubElementsSl = ACTION_SUBELEMENTS_SL;
        }
        return this._actionSubElementsSl;
      },
      set: function set(sl) {
        this._actionSubElementsSl = sl;
      }
    }, {
      key: "bindAction",
      value: function bindAction(button, action) {
        if (this['on' + action]) {
          // handler defined on custom element
          button.onclick = function (event) {
            this['on' + action]();
            this.comp.dispatchEvent(new CustomEvent(action, {
              target: this.comp,
              detail: {
                value: event.target.value
              },
              bubbles: true,
              composed: true
            }));
          }.bind(this);
        } else if (this.comp['on' + action]) {
          // handler defined in implementation
          button.onclick = function (event) {
            this.comp['on' + action]();
            this.comp.dispatchEvent(new CustomEvent(action, {
              target: this.comp,
              detail: {
                value: event.target.value
              },
              bubbles: true,
              composed: true
            }));
          }.bind(this);
        } else {
          // no handler binded just throw and event
          button.onclick = function (event) {
            this.comp.dispatchEvent(new CustomEvent(action, {
              target: this.comp,
              detail: {
                value: event.target.value
              },
              bubbles: true,
              composed: true
            }));
          }.bind(this);
        }
      }
    }, {
      key: "bindActions",
      value: function bindActions(target) {
        target = target ? target : this;
        var buttons = target.querySelectorAll(this.actionSubElementsSl);
        var _iterator7 = _createForOfIteratorHelper$3(buttons),
          _step7;
        try {
          for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
            var button = _step7.value;
            if (button.hasAttribute('action')) {
              var action = button.getAttribute('action');
              this.bindAction(button, action);
            }
            if (button.dataset.action) {
              var _action = button.dataset.action;
              this.bindAction(button, _action);
            }
          }
        } catch (err) {
          _iterator7.e(err);
        } finally {
          _iterator7.f();
        }
      }
    }, {
      key: "subEls",
      get: function get() {
        return [];
      }
    }, {
      key: "clearAllElCache",
      value: function clearAllElCache() {
        var _iterator8 = _createForOfIteratorHelper$3(this.subEls),
          _step8;
        try {
          for (_iterator8.s(); !(_step8 = _iterator8.n()).done;) {
            var el = _step8.value;
            if (this[el]) {
              this[el] = null;
            }
          }
        } catch (err) {
          _iterator8.e(err);
        } finally {
          _iterator8.f();
        }
        this.comp.clearElCache();
      }
    }, {
      key: "removeEl",
      value: function removeEl(el) {
        if (typeof el.remove === 'function') {
          el.remove();
        } else if (el.parentElement) {
          el.parentElement.removeChild(el);
        }
      }
    }]);
  }(EventTarget);

  var SkLocaleEn = {};

  var SkLocaleDe = {
    'Field value is invalid': 'Das ist ungültig',
    'This field is required': 'Dies ist erforderlich',
    'The value is less than required': 'Das muss größer sein',
    'The value is greater than required': 'Das muss weniger sein',
    'The value must me a valid email': 'Das muss eine E-Mail sein',
    'The value must match requirements': 'Dies muss den Anforderungen entsprechen',
    'Ok': 'Ок',
    'Cancel': 'Cancel'
  };

  var SkLocaleRu = {
    'Field value is invalid': 'Ошибка ввода данных',
    'This field is required': 'Это поле обязательное',
    'The value is less than required': 'Значение меньше допустимого',
    'The value is greater than required': 'Значение больше допустимого',
    'The value must me a valid email': 'Значение должно быть email адресом',
    'The value must match requirements': 'Значение должно соответствовать',
    'Ok': 'Ок',
    'Cancel': 'Отмена'
  };

  var SkLocaleCn = {
    'Field value is invalid': '不对',
    'This field is required': '必填',
    'The value is less than required': '太小',
    'The value is greater than required': '太大',
    'The value must me a valid email': '需要电子邮件',
    'The value must match requirements': '不对',
    'Ok': '好的',
    'Cancel': '消除'
  };

  var SkLocaleHi = {
    'Field value is invalid': 'यह अमान्य है',
    'This field is required': 'यह आवश्यक है',
    'The value is less than required': 'यह अधिक होना चाहिए',
    'The value is greater than required': 'यह कम होना चाहिए',
    'The value must me a valid email': 'यह ईमेल होना चाहिए',
    'The value must match requirements': 'यह आवश्यकताओं से मेल खाना चाहिए',
    'Ok': 'Ок',
    'Cancel': 'रद्द करें'
  };

  var LANGS_BY_CODES = {
    'en_US': 'EN',
    'en_UK': 'EN',
    'en': 'EN',
    'EN': 'EN',
    'us': 'EN',
    'ru_RU': 'RU',
    'ru': 'RU',
    'cn': 'CN',
    'CN': 'CN',
    'zh': 'CN',
    'de_DE': 'DE',
    'de': 'DE',
    'DE': 'DE',
    'hi-IN': 'HI',
    'hi_IN': 'HI',
    'HI': 'HI',
    'hi': 'HI'
  };
  var LOCALES = {
    'EN': SkLocaleEn,
    'DE': SkLocaleDe,
    'RU': SkLocaleRu,
    'CN': SkLocaleCn,
    'HI': SkLocaleHi
  };
  var SkLocale = /*#__PURE__*/function () {
    function SkLocale(lang) {
      _classCallCheck(this, SkLocale);
      this.lang = LANGS_BY_CODES[lang];
    }
    return _createClass(SkLocale, [{
      key: "tr",
      value: function tr(string) {
        if (this.lang) {
          return LOCALES[this.lang][string] ? LOCALES[this.lang][string] : string;
        } else {
          return SkLocaleEn[string] ? SkLocaleEn[string] : string;
        }
      }
    }]);
  }();

  function _callSuper$3(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$3() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$3() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$3 = function _isNativeReflectConstruct() { return !!t; })(); }
  var SK_ATTR_CHANGE_EVT = 'skattrchange';
  var SkAttrChangeEvent = /*#__PURE__*/function (_CustomEvent) {
    function SkAttrChangeEvent(options) {
      _classCallCheck(this, SkAttrChangeEvent);
      return _callSuper$3(this, SkAttrChangeEvent, [SK_ATTR_CHANGE_EVT, options]);
    }
    _inherits(SkAttrChangeEvent, _CustomEvent);
    return _createClass(SkAttrChangeEvent);
  }(/*#__PURE__*/_wrapNativeSuper(CustomEvent));

  var JsonPathPlusDriver = /*#__PURE__*/function () {
    function JsonPathPlusDriver() {
      _classCallCheck(this, JsonPathPlusDriver);
    }
    return _createClass(JsonPathPlusDriver, [{
      key: "query",
      value: function query(data, path) {
        return window.JSONPath.JSONPath({
          path: path,
          json: data
        });
      }
    }]);
  }();

  function _createForOfIteratorHelper$2(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$2(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$2(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$2(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$2(r, a) : void 0; } }
  function _arrayLikeToArray$2(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  var JSONPATH_CN = "JsonPath";
  var JSONPATH_PT = "/node_modules/sk-core/src/json-path.js";
  if (window.hasOwnProperty('jsonpath')) {
    if (typeof window.jsonpath.JSONPath === 'function') {
      new window.jsonpath.JSONPath();
    }
  }
  var logger$1 = ConsoleLogger.instance();
  var SkJsonPathDriver = /*#__PURE__*/function () {
    function SkJsonPathDriver() {
      _classCallCheck(this, SkJsonPathDriver);
    }
    return _createClass(SkJsonPathDriver, [{
      key: "libPath",
      get: function get() {
        return window.skJsonPathUrl ? window.skJsonPathUrl : '/node_modules/sk-jsonpath/src/jsonpath.js';
      }
    }, {
      key: "lib",
      value: function lib() {
        if (!this._lib) {
          if (window.hasOwnProperty('jsonpath')) {
            this._lib = typeof window.jsonpath.JSONPath === 'function' ? window.jsonpath.JSONPath() : null;
          } else {
            this.libLoadPromise = new Promise(function (resolve, reject) {
              var _this = this;
              var onLoaded = ResLoader.loadClass('JSONPath', this.libPath, null, false, true);
              onLoaded.then(function () {
                _newArrowCheck(this, _this);
                if (window.hasOwnProperty('jsonpath')) {
                  this._lib = new window.jsonpath.JSONPath();
                  resolve(this._lib);
                } else {
                  reject('Unable to load sk-jsonpath library');
                }
              }.bind(this));
            }.bind(this));
          }
        }
        return this._lib;
      }
    }, {
      key: "query",
      value: function query() {
        var args = arguments;
        var impl = null;
        if (this._lib && typeof this._lib.query === 'function') {
          impl = this._lib;
        }
        if (!impl) {
          if (!this.notLoadedWarnShown) {
            logger$1.warn('JSONPath lib not inited, using fallback impl');
            this.notLoadedWarnShown = true;
          }
          var pathString = args[1] ? args[1].replace('$.', '') : '';
          // fallback simple recursive path drill impl
          var data = args[0];
          var path = pathString.split('.');
          if (path.length == 1) {
            return [data[path[0]]];
          } else {
            var item = data;
            var _iterator = _createForOfIteratorHelper$2(path),
              _step;
            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var token = _step.value;
                item = item[token];
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }
            return item;
          }
        } else {
          var _impl;
          return (_impl = impl).query.apply(_impl, _toConsumableArray(args));
        }
      }
    }]);
  }();
  var JsonPath = /*#__PURE__*/function () {
    function JsonPath() {
      _classCallCheck(this, JsonPath);
    }
    return _createClass(JsonPath, [{
      key: "driClass",
      get: function get() {
        return window.JsonPathPlusEsmDriver ?
        // esm driver bundled at compile time
        window.JsonPathPlusEsmDriver : window.JSONPath && window.JSONPath.JSONPath ?
        // jsonpath-plus library pluged statically at runtime
        JsonPathPlusDriver : SkJsonPathDriver; //fallback
      }
    }, {
      key: "dri",
      get: function get() {
        if (!this._dri) {
          this._dri = new this.driClass();
        }
        return this._dri;
      },
      set: function set(dri) {
        this._dri = dri;
      }
    }, {
      key: "query",
      value: function query() {
        var _this$dri;
        return (_this$dri = this.dri).query.apply(_this$dri, arguments);
      }
    }]);
  }();

  var CONFIG_ATTRS_BY_CC = new Map([['theme', 'theme'], ['basePath', 'base-path'], ['tplPath', 'tpl-path'], ['themePath', 'theme-path'], ['lang', 'lang'], ['styles', 'styles'], ['useShadowRoot', 'use-shadow-root'], ['reflective', 'reflective'], ['genIds', 'gen-ids'], ['rdVarRender', 'rd-var-render'], ['rdCache', 'rd-cache'], ['rdCacheGlobal', 'rd-cache-global'], ['rdTplFmt', 'rd-tpl-fmt'], ['stStore', 'st-store'], ['logLv', 'log-lv'], ['logT', 'log-t']]);
  var StringUtils = /*#__PURE__*/function () {
    function StringUtils() {
      _classCallCheck(this, StringUtils);
    }
    return _createClass(StringUtils, null, [{
      key: "css2CamelCase",
      value:
      /**
       * converts string-in-css-format to camelCase
       * @param str
       * @returns str
       */
      function css2CamelCase(str) {
        return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
          return index === 0 ? word.toLowerCase() : word.toUpperCase();
        }).replace(/\s+/g, '').replace(/-/g, '');
      }
    }, {
      key: "capitalizeFirstLetter",
      value: function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
      }
    }, {
      key: "camelCase2Css",
      value: function camelCase2Css(str) {
        var _this = this;
        if (!StringUtils._camel2CssCache) {
          StringUtils._camel2CssCache = CONFIG_ATTRS_BY_CC;
        }
        if (!StringUtils._camel2CssCache.get(str)) {
          var arr = str.split(/(?=[A-Z])/);
          var cssStr = arr.map(function (item, index) {
            _newArrowCheck(this, _this);
            return item.slice(0, 1).toLowerCase() + item.slice(1, item.length);
          }.bind(this)).join("-");
          StringUtils._camel2CssCache.set(str, cssStr);
        }
        return StringUtils._camel2CssCache.get(str);
      }
    }]);
  }();

  var _this = undefined;
  var hash = function hash(string) {
    _newArrowCheck(this, _this);
    var hash = 0;
    string = string.toString();
    for (var i = 0; i < string.length; i++) {
      hash = (hash << 5) - hash + string.charCodeAt(i) & 0xFFFFFFFF;
    }
    return hash;
  }.bind(undefined);
  var MAX_RECURSION = 3;
  var object = function object(obj, level) {
    _newArrowCheck(this, _this);
    if (obj && typeof obj.getTime === 'function') {
      return obj.getTime();
    }
    var result = 0;
    for (var property in obj) {
      if (Object.hasOwnProperty.call(obj, property)) {
        result += hash(property + value(obj[property], level));
      }
    }
    return result;
  }.bind(undefined);
  var value = function value(_value, level) {
    _newArrowCheck(this, _this);
    if (level && level > MAX_RECURSION) {
      return 0;
    }
    level = level ? level : 0;
    level++;
    var type = _value == undefined ? undefined : _typeof(_value);
    return MAPPER[type] ? MAPPER[type](_value, level) + hash(type) : 0;
  }.bind(undefined);
  var MAPPER = {
    string: hash,
    number: hash,
    "boolean": hash,
    object: object
  };

  function _callSuper$2(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$2() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$2() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$2 = function _isNativeReflectConstruct() { return !!t; })(); }
  function _createForOfIteratorHelper$1(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray$1(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray$1(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray$1(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$1(r, a) : void 0; } }
  function _arrayLikeToArray$1(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  ResLoader.cacheClassDefs(SkLocale);
  var SK_RENDERED_EVT = "skrendered";
  var CONFIG_EL_REF_NAME = "configEl";
  var CONFIG_OBJ_REF_NAME = "skConfig";
  var DISABLED_AN = "disabled";
  var SK_CONFIG_EL_GLOBAL = "skConfigElGlobal";
  var SK_CONFIG_EL_INDIVIDUAL = "skConfigElIndividual";
  var SK_CONFIG_DISABLED = "skConfigElDisabled";
  var JSONPATH_DRI_CN_AN = "jsonpath-dri-cn";
  var JSONPATH_DRI_PATH_AN = "jsonpath-dri-path";
  var ATTR_PLUGINS_AN = "attr-plugins";
  var BASE_PATH_AN = "base-path";
  var BASE_PATH_DEFAULT = "/node_modules/skinny-widgets/src";
  var TPL_PATH_AN = "tpl-path";
  var IMPL_PATH_AN = "impl-path";
  var TPL_VARS_AN = "tpl-vars";
  var THEME_AN = "theme";
  var THEME_DEFAULT = "default";
  var LANG_AN = "lang";
  var LANG_DEFAULT = "en_US";
  var LOG_LEVEL_AN = "log-lv";
  var LOG_TARGET_AN = "log-t";
  var CONFIG_SL_AN = "config-sl";
  var STATE_STORE_AN = "st-store";
  var STATE_AN = "state";
  var RD_NOT_SHARED_AN = "rd-no-shrd";
  var SK_CONFIG_EL_GLOBAL_CN = "sk-config-el-global";
  var SK_CONFIG_EL_INDIVIDUAL_CN = "sk-config-el-individual";
  var USE_SHADOW_ROOT_AN = "use-shadow-root";
  var REFLECTIVE_AN = "reflective";
  var I18N_AN = "i18n";
  var NO_ATTR_EVTS_AN = "no-attr-evts";
  var STORED_CFG_AN = "stored-cfg";
  var STORED_PROPS_AN = "stored-props";
  function _bindAttrPlugins(target) {
    var self = target ? target : this;
    var targetPlugins = target ? target.plugins : this.plugins;
    var plugins = new Map();
    var _iterator = _createForOfIteratorHelper$1(self.attributes),
      _step;
    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var attr = _step.value;
        var cnRes = attr.name.match(/plg-(.*)-cn/);
        if (cnRes) {
          var pluginName = cnRes[1];
          if (!plugins.get(pluginName)) {
            plugins.set(pluginName, {});
          }
          plugins.get(pluginName).className = self.getAttribute(attr.name);
        }
        var pathRes = attr.name.match(/plg-(.*)-path/);
        if (pathRes) {
          var _pluginName = pathRes[1];
          if (!plugins.get(_pluginName)) {
            plugins.set(_pluginName, {});
          }
          plugins.get(_pluginName).path = self.getAttribute(attr.name);
        }
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
    var _iterator2 = _createForOfIteratorHelper$1(plugins),
      _step2;
    try {
      for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
        var _step2$value = _slicedToArray(_step2.value, 2),
          name = _step2$value[0],
          plugin = _step2$value[1];
        if (plugin.className && plugin.path) {
          var dimportOff = self.getAttribute(DIMPORTS_AN) === 'false';
          ResLoader.dynLoad(plugin.className, plugin.path, function (def) {
            targetPlugins.push(new def(target));
          }.bind(target), false, false, false, false, false, false, dimportOff);
        }
      }
    } catch (err) {
      _iterator2.e(err);
    } finally {
      _iterator2.f();
    }
  }
  var SkElement = /*#__PURE__*/function (_HTMLElement) {
    function SkElement() {
      _classCallCheck(this, SkElement);
      return _callSuper$2(this, SkElement, arguments);
    }
    _inherits(SkElement, _HTMLElement);
    return _createClass(SkElement, [{
      key: "impl",
      get: function get() {
        if (!this._impl) {
          var StubImpl = /*#__PURE__*/function (_SkComponentImpl) {
            function StubImpl() {
              _classCallCheck(this, StubImpl);
              return _callSuper$2(this, StubImpl, arguments);
            }
            _inherits(StubImpl, _SkComponentImpl);
            return _createClass(StubImpl);
          }(SkComponentImpl);
          this._impl = new StubImpl(this);
        }
        return this._impl;
      },
      set: function set(impl) {
        this._impl = impl;
      }
    }, {
      key: "theme",
      get: function get() {
        return this.getAttribute(THEME_AN) || THEME_DEFAULT;
      },
      set: function set(theme) {
        return this.setAttribute(THEME_AN, theme);
      }
    }, {
      key: "basePath",
      get: function get() {
        // defaulting for packaged library usage
        return this.getAttribute(BASE_PATH_AN) || BASE_PATH_DEFAULT;
      },
      set: function set(basePath) {
        return this.setAttribute(BASE_PATH_AN, basePath);
      }
    }, {
      key: "tplPath",
      get: function get() {
        return this.getAttribute(TPL_PATH_AN);
      },
      set: function set(tplPath) {
        return this.setAttribute(TPL_PATH_AN, tplPath);
      }
    }, {
      key: "useShadowRoot",
      get: function get() {
        if (this.getAttribute(USE_SHADOW_ROOT_AN) === 'false') {
          return false;
        }
        return true;
      }
    }, {
      key: "lang",
      get: function get() {
        return this.getAttribute(LANG_AN) || LANG_DEFAULT;
      }
    }, {
      key: "locale",
      get: function get() {
        if (!this._locale) {
          if (this.configEl) {
            if (this.configEl.locale) {
              if (this.lang === this.configEl.locale.lang) {
                this._locale = this.configEl.locale;
              } else {
                this._locale = ResLoader.dynLoad("SkLocale", "./sk-locale.js", function (def) {
                  return new def(this.lang);
                }.bind(this), false, null, null, null, null, null, null, true, window["ResLoader"]);
              }
            } else {
              if (this.lang === this.configEl.lang) {
                this._locale = ResLoader.dynLoad("SkLocale", "./sk-locale.js", function (def) {
                  return new def(this.lang);
                }.bind(this), false, null, null, null, null, null, null, true, window["ResLoader"]);
                this.configEl.locale = this.locale;
              } else {
                this._locale = ResLoader.dynLoad("SkLocale", "./sk-locale.js", function (def) {
                  return new def(this.lang);
                }.bind(this), false, null, null, null, null, null, null, true, window["ResLoader"]);
              }
            }
          } else {
            this._locale = ResLoader.dynLoad("SkLocale", "./sk-locale.js", function (def) {
              return new def(this.lang);
            }.bind(this), false, null, null, null, null, null, null, true, window["ResLoader"]);
          }
        }
        return this._locale;
      },
      set: function set(locale) {
        this._locale = locale;
      }
    }, {
      key: "reflective",
      get: function get() {
        return this.getAttribute(REFLECTIVE_AN) || true;
      }
    }, {
      key: "el",
      get: function get() {
        return this.useShadowRoot ? this.shadowRoot : this;
      }

      /**
       * copied to impl and used in SkCommonImpl.renderWithVars method
       * @returns {{}}
       */
    }, {
      key: "tplVars",
      get: function get() {
        if (!this._tplVars) {
          this._tplVars = this.hasAttribute(TPL_VARS_AN) ? JSON.parse(this.getAttribute(TPL_VARS_AN)) : {};
        }
        return this._tplVars;
      },
      set: function set(tplVars) {
        this._tplVars = tplVars;
      }

      /**
       * copied to impl and used in SkCommonImpl.renderWithVars method
       * @returns {{}}
       */
    }, {
      key: "i18n",
      get: function get() {
        if (!this._i18n) {
          this._i18n = {};
          try {
            if (this.configEl) {
              if (this.configEl.i18n) {
                Object.assign(this._i18n, this.configEl.i18n);
              } else if (this.configEl.hasAttribute(I18N_AN)) {
                // sk-element exists by not registred
                Object.assign(this._i18n, JSON.parse(this.configEl.i18n));
              }
            }
          } catch (e) {
            this.logger.error('error loading localization from sk-config element');
          }
          try {
            if (this.hasAttribute(I18N_AN)) {
              Object.assign(this._i18n, JSON.parse(this.getAttribute(I18N_AN)));
            }
          } catch (e) {
            this.logger.error("error loading ".concat(this.constructor.name, " element's own localization"));
          }
          if (this.renderer && this.renderer.translations && _typeof(this.renderer.translations[this.lang]) === 'object') {
            Object.assign(this._i18n, this.renderer.translations[this.lang]);
          }
        }
        return this._i18n;
      },
      set: function set(i18n) {
        this._i18n = i18n;
      }
    }, {
      key: "implClassName",
      get: function get() {
        return this.capitalizeFirstLetter(this.theme) + 'Sk' + this.capitalizeFirstLetter(this.cnSuffix);
      }
    }, {
      key: "implPath",
      get: function get() {
        return this.getAttribute(IMPL_PATH_AN) || "/node_modules/sk-".concat(this.cnSuffix, "-").concat(this.theme, "/src/").concat(this.theme, "-sk-").concat(this.cnSuffix, ".js");
      }
    }, {
      key: "disabled",
      get: function get() {
        return this.hasAttribute(DISABLED_AN);
      },
      set: function set(disabled) {
        if (disabled) {
          this.setAttribute(DISABLED_AN, '');
        } else {
          this.removeAttribute(DISABLED_AN);
        }
      }
    }, {
      key: "logLevels",
      get: function get() {
        return this.hasAttribute(LOG_LEVEL_AN) ? this.getAttribute(LOG_LEVEL_AN) : this.hasAttribute('log_lv') ? this.getAttribute('log_lv') : LOG_LEVEL_DEFAULT;
      },
      set: function set(level) {
        this.setAttribute(LOG_LEVEL_AN, level);
      }
    }, {
      key: "confValOrDefault",
      value: function confValOrDefault(attrName, defaultVal) {
        if (this.hasAttribute(attrName)) {
          return this.getAttribute(attrName);
        } else if (this.configEl && this.configEl.hasAttribute(attrName)) {
          return this.configEl.getAttribute(attrName);
        } else {
          return defaultVal;
        }
      }
    }, {
      key: "confValEnabled",
      value: function confValEnabled(attrName) {
        var result = false;
        if (this.hasAttribute(attrName)) {
          result = this.getAttribute(attrName);
        } else if (this.configEl && this.configEl.hasAttribute(attrName)) {
          result = this.configEl.getAttribute(attrName);
        } else {
          result = false;
        }
        return result !== false && result !== null && result !== undefined && result !== "false" && result !== "no" && result !== "null";
      }
    }, {
      key: "createLogger",
      value: function createLogger(el) {
        var logger;
        if (el.hasAttribute(LOG_TARGET_AN)) {
          var logTarget = el.getAttribute(LOG_TARGET_AN);
          if (logTarget === 'memory') {
            logger = new MemoryLogger();
          } else if (logTarget === 'session') {
            logger = new SessionLogger();
          } else {
            logger = new ConsoleLogger();
          }
        } else {
          logger = new ConsoleLogger();
        }
        return logger;
      }
    }, {
      key: "logger",
      get: function get() {
        if (!this._logger) {
          if (!this.hasAttribute(LOG_LEVEL_AN)) {
            if (this.configEl && this.configEl.logger) {
              this._logger = this.configEl.logger;
            } else {
              if (this.configEl) {
                this._logger = this.createLogger(this.configEl);
                this._logger.levels = this.logLevels = this.configEl.hasAttribute(LOG_LEVEL_AN) ? this.configEl.getAttribute(LOG_LEVEL_AN) : LOG_LEVEL_DEFAULT;
              } else {
                this._logger = ConsoleLogger.instance();
              }
            }
          } else {
            this._logger = this.createLogger(this);
            this._logger.levels = this.logLevels = this.getAttribute(LOG_LEVEL_AN);
          }
          if (this.configEl && !this.configEl.logger) {
            if (this.configEl.getAttribute(LOG_LEVEL_AN) === this.logLevels || !this.configEl.hasAttribute(LOG_LEVEL_AN) && this.logLevels === LOG_LEVEL_DEFAULT) {
              this.configEl.logger = this._logger;
            }
          }
        }
        return this._logger;
      },
      set: function set(log) {
        this._logger = logger;
      }
    }, {
      key: "dynamicImportSupported",
      value: function dynamicImportSupported() {
        try {
          new Function('import("")');
          return true;
        } catch (err) {
          return false;
        }
      }
    }, {
      key: "isInIE",
      value: function isInIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
          return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }
        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
          var rv = ua.indexOf('rv:');
          return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
          return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }
        return false;
      }
    }, {
      key: "initImpl",
      value: function initImpl() {
        var _this = this;
        try {
          var def = Function('return ' + this.implClassName)();
          this._impl = new def(this);
        } catch (_unused) {
          this._implPromise = ResLoader.loadClass(this.implClassName, this.implPath);
          this._implPromise.then(function (m) {
            _newArrowCheck(this, _this);
            if (typeof m[this.implClassName] === 'function') {
              this._impl = new m[this.implClassName](this);
              this._impl.renderImpl();
            } else {
              this.logger.warn("expected constructor for ".concat(this.implClassName, " not found"));
            }
          }.bind(this));
        }
      }
    }, {
      key: "stateStoreBackend",
      get: function get() {
        return this.getAttribute(STATE_STORE_AN) || false;
      }
    }, {
      key: "contentsState",
      get: function get() {
        if (!this.stateStoreBackend) {
          return JSON.parse(this.getAttribute(STATE_AN));
        } else if (this.stateStoreBackend === 'session') {
          return JSON.parse(sessionStorage.getItem(this.cid));
        }
      },
      set: function set(state) {
        if (!this.stateStoreBackend) {
          this.setAttribute(STATE_AN, JSON.stringify(state));
        } else if (this.stateStoreBackend === 'session') {
          this.cid = this.hasAttribute('id') ? this.getAttribute('id') : null;
          if (!this.cid) {
            this.cid = this.renderer.genElId(this);
          }
          sessionStorage.setItem(this.cid, JSON.stringify(state));
        }
      }
    }, {
      key: "configSl",
      get: function get() {
        return this.getAttribute(CONFIG_SL_AN) || this.getAttribute('configSl') || SK_CONFIG_TN;
      }
    }, {
      key: "configEl",
      get: function get() {
        if (!this._configEl) {
          if (!this.hasAttribute(CONFIG_SL_AN) && !this.hasAttribute('configSl')) {
            if (window[CONFIG_EL_REF_NAME]) {
              this._configEl = window[CONFIG_EL_REF_NAME];
            } else {
              var configEl = document.querySelector(this.configSl);
              if (configEl !== null) {
                this._configEl = configEl;
                window[CONFIG_EL_REF_NAME] = this._configEl;
              }
            }
          } else {
            this._configEl = document.querySelector(this.configSl);
          }
        }
        return this._configEl;
      },
      set: function set(configEl) {
        this._configEl = configEl;
      }
    }, {
      key: "subEls",
      get: function get() {
        return [];
      }
    }, {
      key: "renderer",
      get: function get() {
        return this._renderer;
      },
      set: function set(renderer) {
        this._renderer = renderer;
      }
    }, {
      key: "skThemeLoader",
      get: function get() {
        if (this.configEl && this.configEl.skTheme) {
          return Promise.resolve(this.configEl.skTheme);
        } else {
          if (this.configEl) {
            this.configEl.skThemeLoader = SkThemes.byName(this.theme, this.configEl || this);
            return this.configEl.skThemeLoader;
          } else {
            return SkThemes.byName(this.theme, this.configEl || this);
          }
        }
      }
    }, {
      key: "validationMessage",
      get: function get() {
        return this._validationMessage || this.locale.tr('Field value is invalid');
      },
      set: function set(validationMessage) {
        this._validationMessage = validationMessage;
      }
    }, {
      key: "connections",
      get: function get() {
        if (!this._connections) {
          this._connections = this.constructor.connections || [];
        }
        return this._connections;
      },
      set: function set(connections) {
        this._connections = connections;
      }
    }, {
      key: "jsonPath",
      get: function get() {
        if (!this._jsonPath) {
          this._jsonPath = ResLoader.dynLoad(JSONPATH_CN, JSONPATH_PT, function (def) {
            return new def();
          }.bind(this), false, false, false, false, false, false, true, true, JsonPath);
        }
        return this._jsonPath;
      },
      set: function set(jsonPath) {
        this._jsonPath = jsonPath;
      }
    }, {
      key: "storedCfg",
      get: function get() {
        if (!this._storedCfg) {
          if (this.hasAttribute(STORED_CFG_AN)) {
            try {
              this._storedCfg = JSON.parse(this.getAttribute(STORED_CFG_AN));
            } catch (e) {
              this.logger.warn("Error parsing stored-cfg for element", this);
            }
          }
          if (!this._storedCfg) {
            this._storedCfg = {};
          }
        }
        return this._storedCfg;
      },
      set: function set(storedCfg) {
        this._storedCfg = storedCfg;
        this.setAttribute(STORED_CFG_AN, JSON.stringify(this._storedCfg));
      }
    }, {
      key: "toBool",
      value: function toBool(s) {
        var regex = /^\s*(true|1|on)\s*$/i;
        return regex.test(s);
      }
    }, {
      key: "applyToTarget",
      value: function applyToTarget(source, target, targetType, el, value) {
        if (targetType === 'event' || !targetType) {
          // event is default target type
          var event = target ? new CustomEvent(target, {
            detail: {
              value: value
            }
          }) : new Event('click'); // click is default event
          el.dispatchEvent(event);
        } else if (targetType === 'method') {
          el[target].call(el, value);
        } else if (targetType === 'attr') {
          el.setAttribute(target, value);
        } else if (targetType === 'togglattr') {
          if (value && typeof value === 'string' && value.indexOf("|") > 0) {
            var variants = value.split("|");
            if (el.getAttribute(target) === variants[0]) {
              el.setAttribute(target, variants[1]);
            } else {
              el.setAttribute(target, variants[0]);
            }
          } else if (value && value === 'true' || value === 'false') {
            var val = this.toBool(el.getAttribute(target));
            el.setAttribute(target, !val);
          } else {
            if (el.hasAttribute(target)) {
              el.removeAttribute(target);
            } else {
              el.setAttribute(target, value);
            }
          }
        } else if (targetType === 'prop') {
          el[target] = value;
        } else if (targetType === 'togglprop') {
          var _val = this.toBool(value);
          if (el[target] === _val) {
            el[target] = !_val;
          } else {
            el[target] = _val;
          }
        } else if (targetType === 'function') {
          target.call(el, value);
        }
        this['_' + source] = value;
      }
    }, {
      key: "findTarget",
      value: function findTarget(sl) {
        var el = sl ? this.querySelector(sl) || this.el ? this.el.querySelector(sl) : null : null;
        if (!el) {
          el = document.querySelector(sl);
          el = el ? el : sl ? null : this;
        }
        return el;
      }
    }, {
      key: "attrsToJson",
      value: function attrsToJson(attrs, targetEl, css2Camel) {
        var json = {};
        var _iterator3 = _createForOfIteratorHelper$1(attrs),
          _step3;
        try {
          for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
            var attr = _step3.value;
            if (_typeof(attr) === 'object') {
              if (attr.type === 'array' || attr.type === 'object') {
                var key = css2Camel ? this.css2CamelCase(attr.name) : attr.name;
                if (targetEl.hasAttribute(attr.name)) {
                  try {
                    json[key] = JSON.parse(targetEl.getAttribute(attr.name));
                  } catch (_unused2) {
                    this.logger.error('can not parse element attribute value as json');
                  }
                }
              }
            } else {
              if (targetEl.hasAttribute(attr)) {
                var _key = css2Camel ? this.css2CamelCase(attr) : attr;
                json[_key] = targetEl.getAttribute(attr);
              }
            }
          }
        } catch (err) {
          _iterator3.e(err);
        } finally {
          _iterator3.f();
        }
        return json;
      }
    }, {
      key: "setupConnections",
      value: function setupConnections() {
        var _this2 = this;
        if (this.connections.length > 0) {
          var _iterator4 = _createForOfIteratorHelper$1(this.connections),
            _step4;
          try {
            var _loop = function _loop() {
              var connection = _step4.value;
              var from = connection.from,
                sourceType = connection.sourceType,
                source = connection.source,
                to = connection.to,
                targetType = connection.targetType,
                target = connection.target,
                value = connection.value;
              var fromEl = _this2.findTarget(from);
              if (sourceType === 'event' || !sourceType) {
                // event is default source type
                source = source ? source : 'click'; // click is default event
                target = target ? target : 'click';
                if (fromEl) {
                  fromEl.addEventListener(source, function (event) {
                    var toEl = this.findTarget(to);
                    this.applyToTarget(source, target, targetType, toEl, value || event.target.value || event);
                  }.bind(_this2));
                } else {
                  _this2.logger.warn("".concat(from, " element specified in connections not found"));
                }
              } else if (sourceType === 'prop') {
                var descriptor = Object.getOwnPropertyDescriptor(fromEl, source);
                if (descriptor) {
                  Object.defineProperty(fromEl, source, {
                    set: function (value) {
                      descriptor.set(value);
                      var toEl = this.findTarget(to);
                      this.applyToTarget(source, target, targetType, toEl, value);
                    }.bind(_this2),
                    get: function () {
                      return this['_' + source];
                    }.bind(_this2),
                    configurable: true
                  });
                } else {
                  Object.defineProperty(fromEl, source, {
                    set: function (value) {
                      this['_' + source] = value;
                      var toEl = this.findTarget(to);
                      this.applyToTarget(source, target, targetType, toEl, value);
                    }.bind(_this2),
                    get: function () {
                      return this['_' + source];
                    }.bind(_this2),
                    configurable: true
                  });
                }
              } else if (sourceType === 'attr') {
                var observer = new MutationObserver(function () {
                  var toEl = this.findTarget(to);
                  this.applyToTarget(source, target, targetType, toEl, value);
                }.bind(_this2));
                observer.observe(fromEl, {
                  attributes: true
                });
              }
            };
            for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
              _loop();
            }
          } catch (err) {
            _iterator4.e(err);
          } finally {
            _iterator4.f();
          }
        }
      }
    }, {
      key: "onReflection",
      value: function onReflection(event) {
        var _this3 = this;
        this.logger.debug('sk-element configChanged', event);
        this.configEl.reconfigureElement(this, event.detail);
        if (this.impl.contentsState) {
          this.contentsState = JSON.parse(JSON.stringify(this.impl.contentsState));
        }
        if (event.detail.name && event.detail.name === 'theme') {
          this.impl.unmountStyles();
          this.impl.unbindEvents();
          this.impl.clearTplCache();
          this.impl = null;
        }
        this.addEventListener(SK_RENDER_EVT, function (event) {
          _newArrowCheck(this, _this3);
          this.impl.restoreState({
            contentsState: this.contentsState
          });
        }.bind(this));
        this.render();
      }
    }, {
      key: "fromEntries",
      value: function fromEntries(entries) {
        if (!entries || !entries[Symbol.iterator]) {
          throw new Error('SkElement.fromEntries() requires a single iterable argument');
        }
        var obj = {};
        var _iterator5 = _createForOfIteratorHelper$1(entries),
          _step5;
        try {
          for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
            var _step5$value = _slicedToArray(_step5.value, 2),
              key = _step5$value[0],
              value = _step5$value[1];
            obj[key] = value;
          }
        } catch (err) {
          _iterator5.e(err);
        } finally {
          _iterator5.f();
        }
        return obj;
      }
    }, {
      key: "initJsonPath",
      value: function initJsonPath(cn, path) {
        this.jsonPath = new JsonPath();
        var dynImportsOff = this.confValOrDefault(DIMPORTS_AN, null) === "false";
        ResLoader.dynLoad(cn, path, function (def) {
          this.jsonPath.dri = new def();
        }.bind(this), false, false, false, false, false, false, dynImportsOff);
      }
    }, {
      key: "setupJsonPath",
      value: function setupJsonPath() {
        var jsonPathDriCn = this.confValOrDefault(JSONPATH_DRI_CN_AN, false);
        var jsonPathDriPath = this.confValOrDefault(JSONPATH_DRI_PATH_AN, false);
        if (this.hasAttribute(JSONPATH_DRI_CN_AN) && this.hasAttribute(JSONPATH_DRI_PATH_AN)) {
          this.initJsonPath(jsonPathDriCn, jsonPathDriPath);
        } else {
          if (this.configEl) {
            if (this.configEl.jsonPath) {
              this.jsonPath = this.configEl.jsonPath;
            } else {
              if (jsonPathDriCn && jsonPathDriPath) {
                this.initJsonPath(jsonPathDriCn, jsonPathDriPath);
              } else {
                this.jsonPath;
              }
              this.configEl.jsonPath = this.jsonPath;
            }
          } else {
            this.jsonPath;
          }
        }
      }
    }, {
      key: "setupRenderer",
      value: function setupRenderer() {
        // if no renderer were injected, check if we can use shared by config renderer
        if (!this.renderer) {
          if (!this.hasAttribute(RD_NOT_SHARED_AN) || this.configEl && !this.configEl.hasAttribute(RD_NOT_SHARED_AN)) {
            if (this.configEl) {
              if (!this.configEl.renderer) {
                // configEl exists but renderer not yet bootstrapped
                Renderer.configureForElement(this.configEl);
              }
              this.renderer = this.configEl.renderer;
            } else {
              // no configEl, create shared renderer in window
              if (!window.skRenderer) {
                window.skRenderer = new Renderer();
              }
              this.renderer = window.skRenderer;
            }
          }
        }
        // instanciate new render if no one were provided before or element tag has render options defined
        Renderer.configureForElement(this);
      }
    }, {
      key: "skConfigElPolicy",
      value: function skConfigElPolicy() {
        if (document.documentElement && document.documentElement.classList.contains(SK_CONFIG_EL_GLOBAL_CN) || document.body && document.body.classList.contains(SK_CONFIG_EL_GLOBAL_CN)) {
          return SK_CONFIG_EL_GLOBAL;
        } else if (document.documentElement && document.documentElement.classList.contains(SK_CONFIG_EL_INDIVIDUAL_CN) || document.body && document.body.classList.contains(SK_CONFIG_EL_INDIVIDUAL_CN)) {
          return SK_CONFIG_EL_INDIVIDUAL;
        } else {
          return SK_CONFIG_DISABLED;
        }
      }
    }, {
      key: "setupConfigEl",
      value: function setupConfigEl() {
        if (window[CONFIG_OBJ_REF_NAME]) {
          this.config = window[CONFIG_OBJ_REF_NAME];
          var globalConfigElAbsent = false;
          if (!this.configEl) {
            globalConfigElAbsent = true;
            this.configEl = document.createElement('sk-config');
          }
          for (var _i = 0, _Object$keys = Object.keys(this.config); _i < _Object$keys.length; _i++) {
            var propKey = _Object$keys[_i];
            var attrName = this.camelCase2Css(propKey);
            if (!this.configEl.hasAttribute(attrName)) {
              this.configEl.setAttribute(attrName, this.config[propKey]);
            }
          }
          if (globalConfigElAbsent && this.skConfigElPolicy() === SK_CONFIG_EL_GLOBAL) {
            document.body.appendChild(this.configEl);
          }
        }
      }
    }, {
      key: "setup",
      value: function setup() {
        this.setupConfigEl();
        this.setupRenderer();
        this.setupJsonPath();
        var attrPlugins = this.confValOrDefault(ATTR_PLUGINS_AN, false) || this.hasAttribute(ATTR_PLUGINS_AN);
        if (attrPlugins) {
          this.bindAttrPlugins();
        }
        if (this.configEl && this.configEl.configureElement) {
          this.configEl.configureElement(this);
          if (this.reflective && this.reflective !== 'false') {
            this.configEl.addEventListener('configChanged', this.onReflection.bind(this));
          }
        } else {
          if (this.configEl) {
            // sk-config was not registred, use fallback configuration
            for (var _i2 = 0, _Object$keys2 = Object.keys(EL_SHARED_ATTRS); _i2 < _Object$keys2.length; _i2++) {
              var attrName = _Object$keys2[_i2];
              var value = this.getAttribute(attrName);
              if (value === null && this.configEl.getAttribute(attrName) !== null) {
                this.setAttribute(attrName, this.configEl.getAttribute(attrName));
              }
            }
          }
          if (!this.hasAttribute(BASE_PATH_AN)) {
            this.logger.warn(this.constructor.name, " ", "configEl not found or sk-config element not registred, " + "you will have to add base-path and theme(if not default) attributes for each sk element");
          }
        }
        if (this.useShadowRoot) {
          if (!this.shadowRoot) {
            this.attachShadow({
              mode: 'open'
            });
          }
        }
      }
    }, {
      key: "withUpdatesLocked",
      value: function withUpdatesLocked(callback) {
        if (typeof callback === 'function') {
          this.updatesLocked = true;
          callback.call(this);
          this.updatesLocked = false;
        }
      }
    }, {
      key: "storedProps",
      get: function get() {
        if (!this._storedProps) {
          if (this.hasAttribute(STORED_PROPS_AN)) {
            try {
              this._storedProps = JSON.parse(this.getAttribute(STORED_PROPS_AN));
            } catch (e) {
              this.logger.warn('Error parsing stored attrs attribute for element', this);
              this._storedProps = [];
            }
          } else {
            this._storedProps = [];
          }
        }
        return this._storedProps;
      },
      set: function set(props) {
        this._storedProps = props;
      }
    }, {
      key: "loadStoredCfg",
      value: function loadStoredCfg() {
        var _this4 = this;
        if (this.storedProps.length > 0) {
          var _iterator6 = _createForOfIteratorHelper$1(this.storedProps),
            _step6;
          try {
            var _loop2 = function _loop2() {
              var _this5 = this;
              var prop = _step6.value;
              if (_this4.storedCfg[prop]) {
                _this4.withUpdatesLocked(function () {
                  _newArrowCheck(this, _this5);
                  _this4[prop] = _this4.storedCfg[prop];
                }.bind(this));
              }
            };
            for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
              _loop2();
            }
          } catch (err) {
            _iterator6.e(err);
          } finally {
            _iterator6.f();
          }
        }
      }
    }, {
      key: "render",
      value: function () {
        var _render = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee() {
          return regenerator.wrap(function _callee$(_context) {
            while (1) switch (_context.prev = _context.next) {
              case 0:
                this.callPluginHook('onRenderStart');
                if (this.impl) {
                  this.addToRendering();
                  this.impl.renderImpl();
                  //this.impl.bindEvents();
                  /*        this.bindAutoRender();
                          this.callPluginHook('onRenderEnd');
                          this.dispatchEvent(new CustomEvent('skrender', { bubbles: true, composed: true }));*/
                } else {
                  this.bindAutoRender();
                  this.setupConnections();
                  if (this.skConfigElPolicy() === SK_CONFIG_EL_INDIVIDUAL) {
                    this.el.appendChild(this.configEl);
                  }
                  this.callPluginHook('onRenderEnd');
                  this.renderTimest = Date.now();
                  this.dispatchEvent(new CustomEvent('rendered', {
                    bubbles: true,
                    composed: true
                  })); // :DEPRECATED
                  this.dispatchEvent(new SkRenderEvent({
                    bubbles: true,
                    composed: true
                  }));
                }
              case 2:
              case "end":
                return _context.stop();
            }
          }, _callee, this);
        }));
        function render() {
          return _render.apply(this, arguments);
        }
        return render;
      }()
    }, {
      key: "css2CamelCase",
      value: function css2CamelCase(str) {
        return StringUtils.css2CamelCase(str);
      }
    }, {
      key: "camelCase2Css",
      value: function camelCase2Css(str) {
        return StringUtils.camelCase2Css(str);
      }
    }, {
      key: "capitalizeFirstLetter",
      value: function capitalizeFirstLetter(str) {
        return StringUtils.capitalizeFirstLetter(str);
      }
    }, {
      key: "bindByCn",
      value: function bindByCn(attrName, el, type) {
        if (attrName != null) {
          var propName = this.css2CamelCase(attrName);
          var descriptor = Object.getOwnPropertyDescriptor(this, propName);
          if (descriptor) {
            Object.defineProperty(this, propName, {
              set: function (value) {
                descriptor.set(value);
                if (type === 'at') {
                  if (propName.indexOf('_') > 0) {
                    var _propName$split = propName.split('_'),
                      _propName$split2 = _slicedToArray(_propName$split, 2);
                      _propName$split2[0];
                      var target = _propName$split2[1];
                    el.setAttribute(target, value);
                  } else {
                    el.setAttribute(propName, value);
                  }
                } else if (type === 'cb') {
                  var callbackName = 'on' + this.capitalizeFirstLetter(propName) + 'Change';
                  if (typeof this[callbackName] === 'function') {
                    this[callbackName].call(this, el, value);
                  }
                } else {
                  el.innerHTML = value;
                }
                this['_' + propName] = value;
              }.bind(this),
              get: function () {
                return this['_' + propName];
              }.bind(this),
              configurable: true
            });
          } else {
            Object.defineProperty(this, propName, {
              set: function (value) {
                if (type === 'at') {
                  if (propName.indexOf('_') > 0) {
                    var _propName$split3 = propName.split('_'),
                      _propName$split4 = _slicedToArray(_propName$split3, 2);
                      _propName$split4[0];
                      var target = _propName$split4[1];
                    el.setAttribute(target, value);
                  } else {
                    el.setAttribute(propName, value);
                  }
                } else if (type === 'cb') {
                  var callbackName = 'on' + this.capitalizeFirstLetter(propName) + 'Change';
                  if (typeof this[callbackName] === 'function') {
                    this[callbackName].call(this, el, value);
                  }
                } else {
                  el.innerHTML = value;
                }
                this['_' + propName] = value;
              }.bind(this),
              get: function () {
                return this['_' + propName];
              }.bind(this),
              configurable: true
            });
          }
        }
      }
    }, {
      key: "bindAutoRender",
      value: function bindAutoRender(target) {
        target = target ? target : this.el;
        var autoRenderedEls = target.querySelectorAll('[class*="sk-prop-"]');
        var _iterator7 = _createForOfIteratorHelper$1(autoRenderedEls),
          _step7;
        try {
          for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
            var autoEl = _step7.value;
            // bind in and at classed elements contents and attrs to element's attributes or class props
            // if class property with name matched found we bind it
            // if class property with name matched not found we define and bind it
            var inNames = autoEl.className.match('sk-prop-in-(\\S+)');
            if (inNames) {
              // :TODO validate and transform name to oroper attr or prop name
              this.bindByCn(inNames[1], autoEl);
            }
            var atNames = autoEl.className.match('sk-prop-at-(\\S+)');
            if (atNames) {
              // :TODO validate and transform name to oroper attr or prop name
              this.bindByCn(atNames[1], autoEl, 'at');
            }
            var cbNames = autoEl.className.match('sk-prop-cb-(\\S+)');
            if (cbNames) {
              // :TODO validate and transform name to oroper attr or prop name
              this.bindByCn(cbNames[1], autoEl, 'cb');
            }
          }
        } catch (err) {
          _iterator7.e(err);
        } finally {
          _iterator7.f();
        }
      }
    }, {
      key: "initRendering",
      value: function initRendering() {
        if (!window.skRendering) {
          window.skRendering = new Set();
        }
      }
    }, {
      key: "addToRendering",
      value: function addToRendering() {
        this.initRendering();
        window.skRendering.add(this);
      }
    }, {
      key: "removeFromRendering",
      value: function removeFromRendering() {
        if (window.skRendering) {
          window.skRendering["delete"](this);
          if (window.skRendering.size <= 0) {
            window.dispatchEvent(new CustomEvent(SK_RENDERED_EVT));
            window.skRendered = true;
          }
        } else {
          this.logger.warn('No rendering set found');
        }
      }

      /**
       * preload templates by specified by preLoadedTpls array property with names and matching ${name}Path properties
       * tplName must be like itemTpl, itemActiveTpl, buttonTpl, it will be injected in this class with provided name
       * tplName + 'Path' attribute must be defined in class, e.g. itemTplPath, itemActiveTplPath
       * @returns {Promise<unknown>}
       */
    }, {
      key: "preloadTemplates",
      value: (function () {
        var _preloadTemplates = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee2() {
          var _this6 = this;
          return regenerator.wrap(function _callee2$(_context2) {
            while (1) switch (_context2.prev = _context2.next) {
              case 0:
                return _context2.abrupt("return", new Promise(function (resolve) {
                  var _this7 = this;
                  _newArrowCheck(this, _this6);
                  var loadedTpls = [];
                  var tplMap = new Map();
                  // tplName must be like itemTpl, itemActiveTpl, buttonTpl,
                  var _iterator8 = _createForOfIteratorHelper$1(this.preLoadedTpls),
                    _step8;
                  try {
                    for (_iterator8.s(); !(_step8 = _iterator8.n()).done;) {
                      var tplName = _step8.value;
                      var id = this.constructor.name + this.capitalizeFirstLetter(tplName);
                      var tplLoad = this.renderer.mountTemplate(this[tplName + 'Path'], id, this);
                      loadedTpls.push(tplLoad);
                      tplMap.set(id, tplName);
                    }
                  } catch (err) {
                    _iterator8.e(err);
                  } finally {
                    _iterator8.f();
                  }
                  Promise.all(loadedTpls).then(function (loadedTpls) {
                    _newArrowCheck(this, _this7);
                    // inject templates by attr name w/o path
                    var _iterator9 = _createForOfIteratorHelper$1(loadedTpls),
                      _step9;
                    try {
                      for (_iterator9.s(); !(_step9 = _iterator9.n()).done;) {
                        var loadedTpl = _step9.value;
                        var propName = tplMap.get(loadedTpl.getAttribute('id'));
                        this[propName] = loadedTpl;
                      }
                    } catch (err) {
                      _iterator9.e(err);
                    } finally {
                      _iterator9.f();
                    }
                    resolve(loadedTpls);
                  }.bind(this));
                }.bind(this)));
              case 1:
              case "end":
                return _context2.stop();
            }
          }, _callee2, this);
        }));
        function preloadTemplates() {
          return _preloadTemplates.apply(this, arguments);
        }
        return preloadTemplates;
      }())
    }, {
      key: "connectedCallback",
      value: function connectedCallback() {
        var _this8 = this;
        this.initRendering();
        this.loadStoredCfg();
        this.setup();
        if (this.preLoadedTpls && Array.isArray(this.preLoadedTpls) && this.preLoadedTpls.length > 0) {
          this.preloadTemplates().then(function () {
            _newArrowCheck(this, _this8);
            this.render();
          }.bind(this));
        } else {
          this.render();
        }
      }
    }, {
      key: "setCustomValidity",
      value: function setCustomValidity(validity) {
        this.validationMessage = validity;
        this.impl.renderValidation(validity);
      }
    }, {
      key: "flushValidity",
      value: function flushValidity() {
        this.impl.flushValidation();
      }
    }, {
      key: "plugins",
      get: function get() {
        if (!this._plugins) {
          this._plugins = [];
        }
        if (this.constructor.plugins && this.constructor.plugins.length > 0) {
          return this._plugins.concat(this.constructor.plugins);
        } else {
          return this._plugins;
        }
      },
      set: function set(plugins) {
        this._plugins = plugins;
      }
    }, {
      key: "addPlugin",
      value: function addPlugin(plugin) {
        var global = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
        if (global) {
          if (!this.constructor.plugins) {
            this.constructor.plugins = [];
          }
          this.constructor.plugins.push(plugin);
        } else {
          this.plugins.push(plugin);
        }
      }
    }, {
      key: "walkInPlugins",
      value: function walkInPlugins(name, plugins) {
        for (var _len = arguments.length, rest = new Array(_len > 2 ? _len - 2 : 0), _key2 = 2; _key2 < _len; _key2++) {
          rest[_key2 - 2] = arguments[_key2];
        }
        var _iterator10 = _createForOfIteratorHelper$1(plugins),
          _step10;
        try {
          for (_iterator10.s(); !(_step10 = _iterator10.n()).done;) {
            var plugin = _step10.value;
            if (typeof plugin[name] === 'function') {
              var _plugin$name;
              (_plugin$name = plugin[name]).call.apply(_plugin$name, [this].concat(rest));
            } else if (typeof plugin[name] === 'string') {
              var functionName = plugin[name];
              if (typeof this[functionName] === 'function') {
                var _this$functionName;
                (_this$functionName = this[functionName]).call.apply(_this$functionName, [this].concat(rest));
              } else if (this.impl && typeof this.impl[functionName] === 'function') {
                var _this$impl$functionNa;
                (_this$impl$functionNa = this.impl[functionName]).call.apply(_this$impl$functionNa, [this].concat(rest));
              } else if (typeof window[functionName] === 'function') {
                var _window$functionName;
                (_window$functionName = window[functionName]).call.apply(_window$functionName, [this].concat(rest));
              }
            }
          }
        } catch (err) {
          _iterator10.e(err);
        } finally {
          _iterator10.f();
        }
      }
    }, {
      key: "bindAttrPlugins",
      value: function bindAttrPlugins() {
        _bindAttrPlugins.call.apply(_bindAttrPlugins, [this].concat(Array.prototype.slice.call(arguments)));
      }
    }, {
      key: "callPluginHook",
      value: function callPluginHook(name) {
        for (var _len2 = arguments.length, rest = new Array(_len2 > 1 ? _len2 - 1 : 0), _key3 = 1; _key3 < _len2; _key3++) {
          rest[_key3 - 1] = arguments[_key3];
        }
        if (this.configEl && Array.isArray(this.configEl.plugins) && this.configEl.plugins.length > 0) {
          this.walkInPlugins.apply(this, [name, this.configEl.plugins].concat(rest));
        }
        if (Array.isArray(this.plugins) && this.plugins.length > 0) {
          this.walkInPlugins.apply(this, [name, this.plugins].concat(rest));
        }
      }
    }, {
      key: "clearElCache",
      value: function clearElCache() {
        var _iterator11 = _createForOfIteratorHelper$1(this.subEls),
          _step11;
        try {
          for (_iterator11.s(); !(_step11 = _iterator11.n()).done;) {
            var el = _step11.value;
            if (this[el]) {
              this[el] = null;
            }
          }
        } catch (err) {
          _iterator11.e(err);
        } finally {
          _iterator11.f();
        }
      }
    }, {
      key: "whenRendered",
      value: function whenRendered(callback) {
        return this.renderer ? this.renderer.whenElRendered(this, callback) : Renderer.callWhenRendered(this, callback);
      }
    }, {
      key: "renderDeferred",
      get: function get() {
        if (!this._renderDeferred) {
          if (this.renderer.jquery) {
            this._renderDeferred = new this.renderer.jquery.Deferred();
          } else {
            this._renderDeferred = null;
          }
        }
        return this._renderDeferred;
      }
    }, {
      key: "onRendered",
      value: function onRendered(callback) {
        var _this9 = this;
        if (this.renderDeferred) {
          this.renderDeferred.then(function (result) {
            _newArrowCheck(this, _this9);
            callback.call(this, result);
          }.bind(this));
        } else {
          if (!this.renderer.jquery) {
            this.logger.warn('onRendered() requires jquery to be loaded for renderer');
          }
        }
      }
    }, {
      key: "runSerial",
      value: function () {
        var _runSerial = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee3(taskFunctions) {
          var _this10 = this;
          var starterPromise;
          return regenerator.wrap(function _callee3$(_context3) {
            while (1) switch (_context3.prev = _context3.next) {
              case 0:
                starterPromise = Promise.resolve(null);
                _context3.next = 3;
                return taskFunctions.reduce(function (p, def) {
                  var _this11 = this;
                  _newArrowCheck(this, _this10);
                  if (typeof def === 'function') {
                    return p.then(function () {
                      _newArrowCheck(this, _this11);
                      return def.call(this);
                    }.bind(this));
                  } else {
                    var args = def.args ? def.args : [];
                    return p.then(function () {
                      var _def$func;
                      _newArrowCheck(this, _this11);
                      return (_def$func = def.func).call.apply(_def$func, [this].concat(_toConsumableArray(args)));
                    }.bind(this));
                  }
                }.bind(this), starterPromise);
              case 3:
              case "end":
                return _context3.stop();
            }
          }, _callee3, this);
        }));
        function runSerial(_x) {
          return _runSerial.apply(this, arguments);
        }
        return runSerial;
      }()
    }, {
      key: "disconnectedCallback",
      value: function disconnectedCallback() {
        if (this.impl && typeof this.impl.onDisconnected === 'function') {
          this.impl.onDisconnected();
        }
      }
    }, {
      key: "attributeChangedCallback",
      value: function attributeChangedCallback(name, oldValue, newValue) {
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
        if (!this.confValOrDefault(NO_ATTR_EVTS_AN, false)) {
          this.dispatchEvent(new SkAttrChangeEvent({
            detail: {
              name: name,
              oldValue: oldValue,
              newValue: newValue
            }
          }));
        }
      }
    }, {
      key: "focus",
      value: function focus() {
        if (this !== null && this !== void 0 && this.impl.focus && typeof this.impl.focus === 'function') {
          this.impl.focus();
        }
      }

      /**
       * translate string with placeholders in {0} and {{ var }} formats,
       * first one is chosen when more than one extra argument passed and
       * it is not an object. Initial but rendered string returned if translate
       * not found.
       * e.g. this.tr('Hello, {0}, your email is {1}', 'John', 'mail@example.com');
       * e.g. this.tr('Hello, {{ name }}, your email is {{ email }}', { username: 'John', email: 'mail@example.com' });
       * @param target
       * @param placeholders
       * @returns string
       */
    }, {
      key: "tr",
      value: function tr(target) {
        var str = this.i18n[target] ? this.i18n[target] : target;
        return arguments.length > 1 ? this.format.apply(this, [str].concat(_toConsumableArray(Array.prototype.slice.call(arguments, 1)))) : str;
      }
    }, {
      key: "format",
      value: function format(str) {
        var values = arguments.length > 1 ? Array.prototype.slice.call(arguments, 1) : [];
        if (values.length === 1 && _typeof(values[0]) === 'object') {
          return this.renderer.replaceVars(str, values[0]);
        } else {
          return str.replace(/{(\d+)}/g, function (match, number) {
            return typeof values[number] != 'undefined' ? values[number] : match;
          });
        }
      }
    }, {
      key: "loadDataLink",
      value: function () {
        var _loadDataLink = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee4(dataLink, target) {
          var value;
          return regenerator.wrap(function _callee4$(_context4) {
            while (1) switch (_context4.prev = _context4.next) {
              case 0:
                value = null;
                target = target ? target : this;
                if (!(typeof dataLink === 'string')) {
                  _context4.next = 36;
                  break;
                }
                if (!this[dataLink]) {
                  _context4.next = 13;
                  break;
                }
                if (!(typeof target[dataLink] === 'function')) {
                  _context4.next = 10;
                  break;
                }
                _context4.next = 7;
                return target[dataLink].call(this);
              case 7:
                value = _context4.sent;
                _context4.next = 11;
                break;
              case 10:
                value = target[dataLink];
              case 11:
                _context4.next = 36;
                break;
              case 13:
                if (!window[dataLink]) {
                  _context4.next = 23;
                  break;
                }
                if (!(typeof window[dataLink] === 'function')) {
                  _context4.next = 20;
                  break;
                }
                _context4.next = 17;
                return window[dataLink].call(window);
              case 17:
                value = _context4.sent;
                _context4.next = 21;
                break;
              case 20:
                value = window[dataLink];
              case 21:
                _context4.next = 36;
                break;
              case 23:
                value = this.jsonPath.query(this, dataLink)[0];
                if (!value) {
                  _context4.next = 31;
                  break;
                }
                if (!(typeof value === 'function')) {
                  _context4.next = 29;
                  break;
                }
                _context4.next = 28;
                return value.call(this);
              case 28:
                value = _context4.sent;
              case 29:
                _context4.next = 36;
                break;
              case 31:
                value = this.jsonPath.query(window, dataLink)[0];
                if (!(typeof value === 'function')) {
                  _context4.next = 36;
                  break;
                }
                _context4.next = 35;
                return value.call(this);
              case 35:
                value = _context4.sent;
              case 36:
                return _context4.abrupt("return", Promise.resolve(value));
              case 37:
              case "end":
                return _context4.stop();
            }
          }, _callee4, this);
        }));
        function loadDataLink(_x2, _x3) {
          return _loadDataLink.apply(this, arguments);
        }
        return loadDataLink;
      }()
    }]);
  }(/*#__PURE__*/_wrapNativeSuper(HTMLElement));

  function _superPropBase(t, o) {
    for (; !{}.hasOwnProperty.call(t, o) && null !== (t = _getPrototypeOf(t)););
    return t;
  }

  function _get() {
    return _get = "undefined" != typeof Reflect && Reflect.get ? Reflect.get.bind() : function (e, t, r) {
      var p = _superPropBase(e, t);
      if (p) {
        var n = Object.getOwnPropertyDescriptor(p, t);
        return n.get ? n.get.call(arguments.length < 3 ? e : r) : n.value;
      }
    }, _get.apply(null, arguments);
  }

  var Registry = /*#__PURE__*/function () {
    function Registry() {
      _classCallCheck(this, Registry);
      this.registry = {};
    }
    return _createClass(Registry, [{
      key: "getDep",
      value: function getDep(depName) {
        return this.registry[depName];
      }

      /**
       * If you use shared registry beaware deps with same name are not overriding by default, use ov param for that
       * example:
       *
       * window.fmRegistry = window.fmRegistry || new Registry();
       fmRegistry.wire({
          Renderer,
          catalogTr: { val: { foo: 'bar' }},
          FmProductGrid: { def: FmProductGrid, deps: {
                  'renderer': Renderer
              }, is: 'fm-product-grid'}
          });
        * @param definitionName
       * @param map
       * @returns {*}
       */
    }, {
      key: "initDef",
      value: function initDef(definitionName, map) {
        if (!this.registry[definitionName] || map[definitionName].ov) {
          if (map[definitionName] && map[definitionName].defName) {
            // definition
            var def = Function('return ' + map[definitionName].defName)();
            if (map[definitionName].is) {
              this.registry[definitionName] = def;
            } else {
              this.registry[definitionName] = new def();
            }
          } else if (map[definitionName] && map[definitionName].def) {
            // definition
            if (map[definitionName].is) {
              this.registry[definitionName] = map[definitionName].def;
            } else {
              this.registry[definitionName] = new map[definitionName].def();
            }
          } else if (map[definitionName] && map[definitionName].val !== undefined && map[definitionName].val !== null) {
            this.registry[definitionName] = map[definitionName].val;
          } else if (map[definitionName] && map[definitionName].f) {
            // explicit factory
            if (typeof new map[definitionName].f() === 'function') {
              this.registry[definitionName] = map[definitionName].f.call(map[definitionName].f);
            } else {
              console.error('factory must be a function');
            }
          } else {
            // function or constructor
            if (typeof map[definitionName] === 'function') {
              // guess constructor
              try {
                this.registry[definitionName] = new map[definitionName]();
              } catch (_unused) {
                // if not constructor then factory
                console.warn('constructor not provided for ' + definitionName + ', guess this is a factory');
                this.registry[definitionName] = map[definitionName].call(map[definitionName]);
              }
            } else {
              // just assign the link
              this.registry[definitionName] = map[definitionName];
            }
          }
        }
        return this.registry[definitionName];
      }
    }, {
      key: "wireDef",
      value: function wireDef(definitionName, definition, map) {
        this.initDef(definitionName, map);
        if (definition.deps) {
          var depDefs = Object.keys(definition.deps);
          for (var _i = 0, _depDefs = depDefs; _i < _depDefs.length; _i++) {
            var propName = _depDefs[_i];
            var propDef = definition.deps[propName];
            var defName = typeof propDef === 'string' ? propDef : propDef.name;
            var dependency = this.registry[defName];
            if (!dependency) {
              dependency = this.registry[propDef.name] = this.wireDef(defName, map);
              if (!dependency && propDef) {
                // dependency not found in registry, probably inline value
                dependency = propDef;
              }
            }
            if (definition.is) {
              Object.defineProperty(this.registry[definitionName].prototype, propName, {
                value: dependency,
                writable: true
              });
            } else {
              Object.defineProperty(this.registry[definitionName], propName, {
                value: dependency,
                writable: true
              });
            }
          }
        }
        if (definition.bootstrap) {
          definition.bootstrap(this.registry[definitionName]);
        }
        if (definition.is) {
          if (!window.customElements.get(definition.is)) {
            window.customElements.define(definition.is, this.registry[definitionName]);
          }
        }
      }
    }, {
      key: "checkAndWire",
      value: function checkAndWire(definitionName, definition, map) {
        if (!map[definitionName]) {
          try {
            var def = Function('return ' + definitionName)();
            if (def) {
              this.wireDef(definitionName, definition, map);
            } else {
              console.warn(definitionName + ' definition not loaded for wiring');
            }
          } catch (_unused2) {
            console.warn("dependency ".concat(definitionName, " not found"));
          }
        } else {
          this.wireDef(definitionName, definition, map);
        }
      }
    }, {
      key: "wire",
      value: function wire(map, dynImportOff) {
        var _this2 = this;
        var _loop = function _loop() {
          var definitionName = _Object$keys[_i2];
          var definition = map[definitionName];
          if (definition.path) {
            if (!window[definitionName] || definition.forceLoad) {
              ResLoader.loadClass(definitionName, definition.path, function (m) {
                window[definitionName] = m[definitionName];
                this.checkAndWire(definitionName, definition, map);
              }.bind(_this2), false, dynImportOff);
            } else {
              // definition already loaded
              _this2.checkAndWire(definitionName, definition, map);
            }
          } else {
            _this2.checkAndWire(definitionName, definition, map);
          }
        };
        for (var _i2 = 0, _Object$keys = Object.keys(map); _i2 < _Object$keys.length; _i2++) {
          _loop();
        }
      }
    }, {
      key: "dynamicImportSupported",
      value: function dynamicImportSupported() {
        try {
          new Function('import("")');
          return true;
        } catch (err) {
          return false;
        }
      }
    }]);
  }();

  function _callSuper$1(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$1() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct$1() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct$1 = function _isNativeReflectConstruct() { return !!t; })(); }
  function _superPropGet(t, o, e, r) { var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e); return 2 & r && "function" == typeof p ? function (t) { return p.apply(e, t); } : p; }
  var CompRegistry = /*#__PURE__*/function (_Registry) {
    function CompRegistry() {
      _classCallCheck(this, CompRegistry);
      return _callSuper$1(this, CompRegistry, arguments);
    }
    _inherits(CompRegistry, _Registry);
    return _createClass(CompRegistry, [{
      key: "initDef",
      value:
      /**
       * Overriden method to wire one definition, adds connections and plugins property support
       * @param definitionName
       * @param map
       * @returns {*}
       */
      function initDef(definitionName, map) {
        this.registry[definitionName] = _superPropGet(CompRegistry, "initDef", this, 3)([definitionName, map]);
        if (map[definitionName] && map[definitionName].connections) {
          this.registry[definitionName].prototype.connections = map[definitionName].connections;
        }
        if (map[definitionName] && map[definitionName].plugins) {
          this.registry[definitionName].prototype.plugins = map[definitionName].plugins;
        }
        return this.registry[definitionName];
      }
    }]);
  }(Registry);

  function _createForOfIteratorHelper(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
  function _unsupportedIterableToArray(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray(r, a) : void 0; } }
  function _arrayLikeToArray(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
  function _callSuper(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
  function _isNativeReflectConstruct() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct = function _isNativeReflectConstruct() { return !!t; })(); }
  var REG_ITEM_TAG_NAME = 'sk-registry-item';
  var CONN_ITEM_TAG_NAME = 'sk-registry-connection';
  var CONN_ITEM_ATTRS = ['from', 'source-type', 'source', 'to', 'target-type', 'target', 'value'];
  var SkRegistry = /*#__PURE__*/function (_SkElement) {
    function SkRegistry() {
      _classCallCheck(this, SkRegistry);
      return _callSuper(this, SkRegistry, arguments);
    }
    _inherits(SkRegistry, _SkElement);
    return _createClass(SkRegistry, [{
      key: "itemTn",
      get: function get() {
        return REG_ITEM_TAG_NAME;
      }
    }, {
      key: "connectionTn",
      get: function get() {
        return CONN_ITEM_TAG_NAME;
      }
    }, {
      key: "buildDeps",
      value: function buildDeps() {
        this.deps = new Set();
        var items = this.querySelectorAll(this.itemTn);
        var _iterator = _createForOfIteratorHelper(items),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var item = _step.value;
            var dep = this.attrsToJson(['key', 'is', 'defName', {
              name: 'val',
              type: 'object'
            }, {
              name: 'plugins',
              type: 'array'
            }, {
              name: 'deps',
              type: 'object'
            }], item, true);
            if (item.hasAttribute('plugins')) {
              dep.plugins = JSON.parse(item.getAttribute('plugins'));
            }
            if (item.hasAttribute('full-path')) {
              dep.path = item.getAttribute('full-path');
            } else {
              if (item.hasAttribute('path') && this.basePath) {
                if (this.basePath) {
                  dep.path = this.basePath + '/' + item.getAttribute('path');
                } else {
                  dep.path = item.getAttribute('path');
                }
              }
            }
            if (item.hasAttribute('force-load')) {
              dep.forceLoad = item.getAttribute('force-load');
            }
            //dep.def = (Function('return ' + item.getAttribute("def")))();
            var depConnections = [];
            var connections = item.querySelectorAll(this.connectionTn);
            var _iterator2 = _createForOfIteratorHelper(connections),
              _step2;
            try {
              for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                var connection = _step2.value;
                var depConnection = this.attrsToJson(CONN_ITEM_ATTRS, connection, true);
                depConnections.push(depConnection);
              }
            } catch (err) {
              _iterator2.e(err);
            } finally {
              _iterator2.f();
            }
            if (depConnections.length > 0) {
              dep.connections = depConnections;
            }
            this.deps[dep.key] = dep;
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      }
    }, {
      key: "setup",
      value: function setup() {
        //this.setupRenderer();
        if (this.configEl && this.configEl.configureElement) {
          this.configEl.configureElement(this);
          if (this.reflective && this.reflective !== 'false') {
            this.configEl.addEventListener('configChanged', this.onReflection.bind(this));
          }
        } else {
          if (this.configEl && this.configEl.hasAttribute('base-path')) {
            this.setAttribute('base-path', this.configEl.getAttribute('base-path'));
          }
          if (!this.hasAttribute('base-path')) {
            this.logger.warn(this.constructor.name, " ", "configEl not found or sk-config element not registred, " + "you will have to add base-path and theme(if not default) attributes for each sk element");
          }
          if (this.configEl && this.configEl.hasAttribute('dimport')) {
            this.setAttribute('dimport', this.configEl.getAttribute('dimport'));
          }
        }
        /*        if (this.useShadowRoot) {
                    if (! this.shadowRoot) {
                        this.attachShadow({mode: 'open'});
                    }
                }*/
      }
    }, {
      key: "loadDepsAndCons",
      value: function loadDepsAndCons() {
        this.buildDeps();
        this.registry = window.registry || new CompRegistry();
        var dynImportOff = this.hasAttribute('dimport') && this.getAttribute('dimport') === 'false';
        this.registry.wire(this.deps, dynImportOff);
        this.setupConnections();
      }
    }, {
      key: "connections",
      get: function get() {
        var _this = this;
        if (!this._connections) {
          var connections = Array.from(this.querySelectorAll(this.connectionTn)).filter(function (ele) {
            _newArrowCheck(this, _this);
            return ele.parentElement === this;
          }.bind(this));
          var connectionsJson = [];
          var _iterator3 = _createForOfIteratorHelper(connections),
            _step3;
          try {
            for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
              var connection = _step3.value;
              connectionsJson.push(this.attrsToJson(CONN_ITEM_ATTRS, connection, true));
            }
          } catch (err) {
            _iterator3.e(err);
          } finally {
            _iterator3.f();
          }
          this._connections = connectionsJson;
        }
        return this._connections;
      }
    }, {
      key: "connectedCallback",
      value: function connectedCallback() {
        this.setup();
        if (this.hasAttribute('after-content')) {
          window.addEventListener('DOMContentLoaded', this.loadDepsAndCons.bind(this));
        } else {
          this.loadDepsAndCons();
        }
      }
    }]);
  }(SkElement);

  var AUTOREG_ATTR_NAME = 'sk-auto-reg';
  var AUTOREG_TAG_NAME = 'sk-registry';
  var regSkRegistry = function regSkRegistry() {
    var attrName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : AUTOREG_ATTR_NAME;
    var tag = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : AUTOREG_TAG_NAME;
    var regCl = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : SkRegistry;
    if (document && document.body && document.body.classList.contains(attrName) || document.documentElement && document.documentElement.classList.contains(attrName)) {
      customElements.define(tag, regCl);
    }
  };
  regSkRegistry();

  exports.AUTOREG_ATTR_NAME = AUTOREG_ATTR_NAME;
  exports.AUTOREG_TAG_NAME = AUTOREG_TAG_NAME;
  exports.regSkRegistry = regSkRegistry;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=sk-registry-init-bundle.js.map
